<?php
if (!function_exists('createResponse')) {
    function createResponse($success, $message, $data = [], $json = true) 
    {
        $array = [
            "success" => $success,
            "message" => $message,
            "data" => $data
        ];

        return $json ? response()->json($array) : $array;

    }
}
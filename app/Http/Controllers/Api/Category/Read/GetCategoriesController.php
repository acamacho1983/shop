<?php

namespace App\Http\Controllers\Api\Category\Read;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Alvaro\Shop\Category\Application\Read\GetCategoriesUseCase;

class GetCategoriesController extends Controller
{
    /**
     * Get list categories.
     *     
     * @return \Illuminate\Http\Response
     */
    public function __invoke(GetCategoriesUseCase $useCase)
    {
        return $useCase->execute();
    }
}

<?php

namespace App\Http\Controllers\Api\Client\Create;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Requests\Client\ClientRequest;
use Alvaro\Shop\Client\Application\Create\CreateClientUseCase;
use Alvaro\Shop\Client\Domain\ClientEntity;
use Alvaro\Shop\Client\Domain\ValueObject\ClientId;
use Alvaro\Shop\Client\Domain\ValueObject\ClientName;
use Alvaro\Shop\Client\Domain\ValueObject\ClientContactName;
use Alvaro\Shop\Client\Domain\ValueObject\ClientNif;
use Alvaro\Shop\Client\Domain\ValueObject\ClientEmail;
use Alvaro\Shop\Client\Domain\ValueObject\ClientMovil;
use Alvaro\Shop\Client\Domain\ValueObject\ClientPhone;
use Alvaro\Shop\Client\Domain\ValueObject\ClientWeb;
use Alvaro\Shop\Client\Domain\ValueObject\ClientCountry;
use Alvaro\Shop\Client\Domain\ValueObject\ClientProvince;
use Alvaro\Shop\Client\Domain\ValueObject\ClientCity;
use Alvaro\Shop\Client\Domain\ValueObject\ClientAddress;
use Alvaro\Shop\Client\Domain\ValueObject\ClientCp;
use Alvaro\Shop\Client\Domain\ValueObject\ClientStatusId;
use Alvaro\Shop\Client\Domain\ValueObject\ClientComments;
use Alvaro\Shop\Client\Infraestructure\Model\ClientDAO;

class CreateClientController extends Controller
{
    /**
     * Create new client.
     *     
     * @return \Illuminate\Http\Response
     */
    public function __invoke(ClientRequest $request, CreateClientUseCase $useCase)    
    {
        $message = "Datos guardados correctamente";
        $success = true;
        $clientInsert = null;

        DB::beginTransaction();
        try {
            $clientInsert = $useCase->execute(
                $request->name, 
                $request->contact_name, 
                $request->nif, 
                $request->email, 
                $request->movil, 
                $request->phone, 
                $request->web, 
                $request->country, 
                $request->province, 
                $request->city, 
                $request->address, 
                $request->cp, 
                $request->status_id,                 
                $request->comments
            );
            DB::commit();
        } catch (\Throwable $error) {
            $success = false;
            $message = $error->getMessage();
            DB::rollback();            
        }        

        return createResponse($success, $message, ['id' => $clientInsert ? $clientInsert->id : null]);
    }
}

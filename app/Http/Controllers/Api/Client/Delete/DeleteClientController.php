<?php

namespace App\Http\Controllers\Api\Client\Delete;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Alvaro\Shop\Client\Infraestructure\Model\ClientDAO;
use Alvaro\Shop\Client\Application\Delete\DeleteClientUseCase;

class DeleteClientController extends Controller
{
    /**
     * Delete client.
     *     
     * @return \Illuminate\Http\Response
     */    
    public function __invoke(ClientDAO $client, DeleteClientUseCase $useCase)
    { 
        $message = "Datos eliminados correctamente";
        $success = true;

        DB::beginTransaction();
        try {
            $useCase->execute($client->id); 
            DB::commit();            
        } catch (\Throwable $error) {
            $success = false;
            $message = $error;
            DB::rollback();            
        }        

        return createResponse($success, $message); 

    }
}

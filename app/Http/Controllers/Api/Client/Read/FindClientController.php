<?php

namespace App\Http\Controllers\Api\Client\Read;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Alvaro\Shop\Client\Infraestructure\Model\ClientDAO;
use Alvaro\Shop\Client\Application\Read\FindClientUseCase;

class FindClientController extends Controller
{
    /**
     * Find client by id.
     *     
     * @return \Illuminate\Http\Response
     */
    public function __invoke(ClientDAO $client, FindClientUseCase $useCase)
    {
        return $useCase->execute($client->id); 
    }
}

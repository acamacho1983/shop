<?php

namespace App\Http\Controllers\Api\Client\Read;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Alvaro\Shop\Client\Application\Read\GetClientsUseCase;

class GetClientsController extends Controller
{
    /**
     * Get list clients.
     *     
     * @return \Illuminate\Http\Response
     */
    public function __invoke(GetClientsUseCase $useCase, Request $request)    
    { 
        return $useCase->execute($request);
    }
}

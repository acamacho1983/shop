<?php

namespace App\Http\Controllers\Api\Client\Update;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Requests\Client\ClientRequest;
use Alvaro\Shop\Client\Infraestructure\Model\ClientDAO;
use Alvaro\Shop\Client\Application\Update\UpdateClientUseCase;

class UpdateClientController extends Controller
{
    /**
     * Update client.
     *     
     * @return \Illuminate\Http\Response
     */
    public function __invoke(ClientDAO $client, ClientRequest $request, UpdateClientUseCase $useCase)
    {                
        $message = "Datos guardados correctamente";
        $success = true;

        DB::beginTransaction();
        try {
            $clientUpdate = $useCase->execute(
                $client->id, 
                $request->name, 
                $request->contact_name, 
                $request->nif, 
                $request->email, 
                $request->movil, 
                $request->phone, 
                $request->web, 
                $request->country, 
                $request->province, 
                $request->city, 
                $request->address, 
                $request->cp, 
                $request->status_id,                 
                $request->comments
            ); 
            DB::commit();            
        } catch (\Throwable $error) {
            $success = false;
            $message = $error->getMessage(); 
            DB::rollback();            
        }        

        return createResponse($success, $message, ['id' => $clientUpdate ? $clientUpdate->id : null]);     

    }
}

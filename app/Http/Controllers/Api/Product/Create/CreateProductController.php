<?php

namespace App\Http\Controllers\Api\Product\Create;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Requests\Product\ProductRequest;
use Alvaro\Shop\Product\Application\Create\CreateProductUseCase;
use Alvaro\Shop\Product\Domain\ProductEntity;
use Alvaro\Shop\Product\Domain\ValueObject\ProductId;
use Alvaro\Shop\Product\Domain\ValueObject\ProductReference;
use Alvaro\Shop\Product\Domain\ValueObject\ProductDescription;
use Alvaro\Shop\Product\Domain\ValueObject\ProductAmount;
use Alvaro\Shop\Product\Infraestructure\Model\ProductDAO;

class CreateProductController extends Controller
{
    /**
     * Create new product.
     *     
     * @return \Illuminate\Http\Response
     */
    public function __invoke(ProductRequest $request, CreateProductUseCase $useCase)    
    {
        $message = "Datos guardados correctamente";
        $success = true;
        $productInsert = null;

        DB::beginTransaction();
        try {
            $productInsert = $useCase->execute(
                $request->name, 
                $request->description, 
                $request->amount, 
                $request->dimensions, 
                $request->weight, 
                $request->category_id, 
                $request->status_id, 
                $request->comments
            );
            DB::commit();
        } catch (\Throwable $error) {
            $success = false;
            $message = $error->getMessage();
            DB::rollback();            
        }        

        return createResponse($success, $message, ['id' => $productInsert ? $productInsert->id : null]);
    }
}

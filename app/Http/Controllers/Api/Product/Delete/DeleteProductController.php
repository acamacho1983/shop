<?php

namespace App\Http\Controllers\Api\Product\Delete;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Alvaro\Shop\Product\Infraestructure\Model\ProductDAO;
use Alvaro\Shop\Product\Application\Delete\DeleteProductUseCase;

class DeleteProductController extends Controller
{
    /**
     * Delete product.
     *     
     * @return \Illuminate\Http\Response
     */    
    public function __invoke(ProductDAO $product, DeleteProductUseCase $useCase)
    { 
        $message = "Datos eliminados correctamente";
        $success = true;

        DB::beginTransaction();
        try {
            $useCase->execute($product->id); 
            DB::commit();            
        } catch (\Throwable $error) {
            $success = false;
            $message = $error;
            DB::rollback();            
        }        

        return createResponse($success, $message); 

    }
}

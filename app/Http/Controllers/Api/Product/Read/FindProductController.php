<?php

namespace App\Http\Controllers\Api\Product\Read;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Alvaro\Shop\Product\Infraestructure\Model\ProductDAO;
use Alvaro\Shop\Product\Application\Read\FindProductUseCase;

class FindProductController extends Controller
{
    /**
     * Find product by id.
     *     
     * @return \Illuminate\Http\Response
     */
    public function __invoke(ProductDAO $product, FindProductUseCase $useCase)
    {
        return $useCase->execute($product->id); 
    }
}

<?php

namespace App\Http\Controllers\Api\Product\Read;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Alvaro\Shop\Product\Application\Read\GetProductsUseCase;

class GetProductsController extends Controller
{
    /**
     * Get list products.
     *     
     * @return \Illuminate\Http\Response
     */
    public function __invoke(GetProductsUseCase $useCase, Request $request)
    { 
        return $useCase->execute($request);
    }
}

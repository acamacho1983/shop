<?php

namespace App\Http\Controllers\Api\Product\Update;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Requests\Product\ProductRequest;
use Alvaro\Shop\Product\Infraestructure\Model\ProductDAO;
use Alvaro\Shop\Product\Application\Update\UpdateProductUseCase;

class UpdateProductController extends Controller
{
    /**
     * Get update product.
     *     
     * @return \Illuminate\Http\Response
     */
    public function __invoke(ProductDAO $product, ProductRequest $request, UpdateProductUseCase $useCase)
    {                
        $message = "Datos guardados correctamente";
        $success = true;

        DB::beginTransaction();
        try {
            $productUpdate = $useCase->execute(
                $product->id, 
                $request->name, 
                $request->description, 
                $request->amount, 
                $request->dimensions, 
                $request->weight, 
                $request->category_id, 
                $request->status_id, 
                $request->comments
            ); 
            DB::commit();            
        } catch (\Throwable $error) {
            $success = false;
            $message = $error->getMessage(); 
            DB::rollback();            
        }        

        return createResponse($success, $message, ['id' => $productUpdate ? $productUpdate->id : null]);     

    }
}

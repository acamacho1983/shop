<?php

namespace App\Http\Controllers\Api\Status\Read;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Alvaro\Shop\Status\Application\Read\GetStatusesUseCase;

class GetStatusesController extends Controller
{
    /**
     * Get list statuses.
     *     
     * @return \Illuminate\Http\Response
     */
    public function __invoke(GetStatusesUseCase $useCase, Request $request)
    { 
        return $useCase->execute($request);
    }
}

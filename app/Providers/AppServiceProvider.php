<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    private $bindingInterfaces = [
        \Alvaro\Shop\Product\Domain\ProductRepositoryInterface::class => \Alvaro\Shop\Product\Infraestructure\Persistence\EloquentProductRepository::class,
        \Alvaro\Shop\Client\Domain\ClientRepositoryInterface::class => \Alvaro\Shop\Client\Infraestructure\Persistence\EloquentClientRepository::class,
        \Alvaro\Shop\Category\Domain\CategoryRepositoryInterface::class => \Alvaro\Shop\Category\Infraestructure\Persistence\EloquentCategoryRepository::class, 
        \Alvaro\Shop\Status\Domain\StatusRepositoryInterface::class => \Alvaro\Shop\Status\Infraestructure\Persistence\EloquentStatusRepository::class
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->bindingInterfaces as $interface => $implementation) {
            $this->app->bind($interface, $implementation);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

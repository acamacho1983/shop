<?php

namespace App\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'min:3', 'max:150'], 
            'contact_name' => ['required', 'min:3', 'max:255'], 
            'nif' => ['required', 'max:15'],
            'email' => ['max:150'], 
            'movil' => ['max:50'], 
            'phone' => ['max:50'], 
            'web' => ['max:150'], 
            'country' => ['max:100'], 
            'province' => ['max:150'], 
            'city' => ['max:200'], 
            'address' => ['max:255'], 
            'cp' => ['max:5'], 
            'status_id' => 'required|exists:statuses,id',
        ];
    }
}

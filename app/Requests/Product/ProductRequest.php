<?php

namespace App\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'min:3', 'max:150'], 
            'description' => ['required', 'min:3', 'max:255'], 
            'amount' => ['required', 'numeric', 'gt:0', 'between:0,9999999999.99'],
            'dimensions' => ['max:150'], 
            'weight' => ['max:150'],             
            'category_id' => 'required|exists:categories,id',
            'status_id' => 'required|exists:statuses,id',
        ];
    }
}

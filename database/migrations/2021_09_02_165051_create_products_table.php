<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id(); 
            $table->string('reference', 20); 
            $table->string('name', 150); 
            $table->string('description', 255); 
            $table->decimal('amount'); 
            $table->string('dimensions', 150)->nullable(); 
            $table->string('weight', 150)->nullable(); 
            $table->text('comments')->nullable(); 
            $table->string('photo', 50)->nullable(); 
            $table->bigInteger('category_id')->unsigned()->index('IDX_products_category_id'); 
            $table->bigInteger('status_id')->unsigned()->index('IDX_products_status_id'); 
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('category_id', 'FK_products__category_id')->references('id')->on('categories')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('status_id', 'FK_products__status_id')->references('id')->on('statuses')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

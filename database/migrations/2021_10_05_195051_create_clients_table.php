<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id(); 
            $table->string('name', 150); 
            $table->string('contact_name', 255); 
            $table->string('nif', 15); 
            $table->string('email', 150)->nullable(); 
            $table->string('movil', 50)->nullable(); 
            $table->string('phone', 50)->nullable(); 
            $table->string('web', 150)->nullable(); 
            $table->string('country', 100)->nullable(); 
            $table->string('province', 150)->nullable(); 
            $table->string('city', 200)->nullable(); 
            $table->string('address', 255)->nullable(); 
            $table->string('cp', 5)->nullable(); 
            $table->text('comments')->nullable(); 
            $table->string('photo', 50)->nullable(); 
            $table->bigInteger('status_id')->unsigned()->index('IDX_products_status_id'); 
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('status_id', 'FK_clients__status_id')->references('id')->on('statuses')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}

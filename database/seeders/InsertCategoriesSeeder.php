<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
Use Carbon\Carbon;

class InsertCategoriesSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * php artisan db:seed --class=InsertCategoriesSeeder
     * 
     * @return void
     */
    public function run()
    {
        $categories = [
            [                
                'description' => 'Puertas', 
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [                
                'description' => 'Tarimas', 
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [                
                'description' => 'Tableros', 
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], 
            [                
                'description' => 'Molduras y listones', 
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], 
            [                
                'description' => 'Celosías', 
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]  
        ];        
        
        DB::table('categories')->insert($categories);
    }
}

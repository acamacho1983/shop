<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
Use Carbon\Carbon;

class InsertClientsSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * php artisan db:seed --class=InsertClientsSeeder
     * 
     * @return void
     */
    public function run()
    {
        $clients = [
            [                
                'name' => 'Ebanistería Jose Luis Pérez',
                'contact_name' => 'Jose Luis Pérez Torres',
                'nif' => '75987654M',
                'email' => 'joseluisperez@nomail.es',
                'movil' => '654654654',
                'phone' => '951951951',
                'web' => 'www.joseluisperez.com',
                'country' => 'España',
                'province' => 'Málaga',
                'city' => 'Málaga',
                'address' => 'Avenida de Andalucía, 2',
                'cp' => '29004',                
                'comments' => 'Llamar por las tardes, por las mañanas no está disponible.', 
                'status_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], 
            [                
                'name' => 'FGS Ebanistería',
                'contact_name' => 'Fernando Gómez Sánchez',
                'nif' => '75987352J',
                'email' => 'fernandogomez@nomail.es',
                'movil' => '654321321',
                'phone' => '951963987',
                'web' => null,
                'country' => 'España',
                'province' => 'Málaga',
                'city' => 'Torremolinos',
                'address' => 'Calle San Miguel, 20',
                'cp' => '29204', 
                'comments' => 'Se le puede llamar a cualquier hora, siempre está disponible',                 
                'status_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], 
            [                
                'name' => 'Carpintería y Ebanistería Rodrigo Gutiérrez',
                'contact_name' => 'Rodrigo Gutiérrez',
                'nif' => '47501147H',
                'email' => 'rodrigogutierrez@nomail.es',
                'movil' => '633622611',
                'phone' => '966933988',
                'web' => 'www.rodrigogutierrez',
                'country' => 'España',
                'province' => 'Sevilla',
                'city' => 'Sevilla',
                'address' => 'Calle Eduardo Dato, 12',
                'cp' => '41005', 
                'comments' => null, 
                'status_id' => 4,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ] 
        ];        
        
        DB::table('clients')->insert($clients);
    }
}

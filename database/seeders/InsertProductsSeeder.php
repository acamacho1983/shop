<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
Use Carbon\Carbon;

class InsertProductsSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * php artisan db:seed --class=InsertProductsSeeder
     * 
     * @return void
     */
    public function run()
    {
        $products = [
            [
                'reference' => 'PRO-000001',
                'name' => 'Block Mod. 3000 TM y 11000 TM',
                'description' => 'Puertas disponibles en todo tipo de maderas.',
                'amount' => 100,
                'dimensions' => '72,5 x 203 centímetros',
                'weight' => '30 kg',
                'comments' => 'Puerta de paso disponible en todo tipo de maderas y colores.', 
                'category_id' => 1,
                'status_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], 
            [
                'reference' => 'PRO-000002',
                'name' => 'Block Mod. 5LS y 3000',
                'description' => 'Block Mod. Lacada fresada 5LS y 3000 Lacquered',
                'amount' => 9,
                'dimensions' => '72,5 x 203 centímetros',
                'weight' => '32 kg',
                'comments' => 'Bajo demanda puede realizarse cualquier diseño en el fresado de las puertas.',                
                'category_id' => 1,
                'status_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], 
            [
                'reference' => 'PRO-000003',
                'name' => 'Aglomerado crudo (Arce Canadá)',
                'description' => 'Aglomerado crudo (Arce Canadá)',
                'amount' => 10,
                'dimensions' => '2440 x 1220 x 16 milímetros',
                'weight' => null,
                'comments' => 'Aglomerado crudo (Arce Canadá). Válido para puertas de armario correderas.', 
                'category_id' => 3,
                'status_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], 
            [
                'reference' => 'PRO-000004',
                'name' => 'MH-06 Cuadro 10 mm. Inclinada (151 x 62)',
                'description' => 'MH-06 Cuadro 10 mm. Inclinada en varias maderas (Pino, Pino Melis, Haya, Roble e Iroko)',
                'amount' => 20,
                'dimensions' => '151 x 62 cm',
                'weight' => '110 gramos',
                'comments' => 'Celosía MH-06 (huecos 10 mm.) Machihembrada, enrasada o ensamblada. Malla inclinada. Grosor: 5 mm. Disponible en madera de Pino, Pino Melis, Haya, Roble e Iroko, (otras maderas consultar). Medidas: 200x100, 220x84, 200x38 y 151x62 cm.', 
                'category_id' => 5,
                'status_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], 
            [
                'reference' => 'PRO-000005',
                'name' => '222-MH Estrella 20 mm. Inclinada (220 x 84)',
                'description' => '222-MH Estrella 20 mm. Inclinada (Pino, Pino Melis)',
                'amount' => 30,
                'dimensions' => '220 x 84 cm',
                'weight' => '110 gramos',
                'comments' => 'Celosía R/222-MH Estrella (huecos 20 mm.) Machihembrada,enrasada o ensamblada. Malla inclinada. Grosor: 5 y 9 mm. Disponible en madera de Pino y Pino Melis (otras maderas consultar). Medidas: 200x100 y 220x84 cm.', 
                'category_id' => 5,
                'status_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], 
            [
                'reference' => 'PRO-000006',
                'name' => 'MH-07 HINDÚ 35 mm. Vertical (200 x 100)',
                'description' => 'MH-07 HINDÚ 35 mm. Vertical (Pino, Pino Melis, Haya, Roble e Iroko)',
                'amount' => 40,
                'dimensions' => '200 x 100 cm',
                'weight' => null,
                'comments' => 'Celosía MH-07 HINDÚ (huecos 35 mm.) Machihembrada,enrasada o ensamblada. Malla vertical. Grosor: 16 mm. Disponible en madera de Pino, Pino Melis, Haya, Roble e Iroko, (otras maderas consultar). Medidas: 200x90 y 200x100 cm.', 
                'category_id' => 5,
                'status_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], 
            [
                'reference' => 'PRO-000007',
                'name' => 'Tablero alistonado de Roble',
                'description' => 'Tablero alistonado de Roble. TIPO PARQUET.',
                'amount' => 60,
                'dimensions' => '3300 x 1220 x 19 mm',
                'weight' => '10 kg',
                'comments' => 'Todos nuestros tableros van encolados con cola marina anti-humedad y con la normativa CE Norma EN-204 / Grupo de esfuerzo D/4', 
                'category_id' => 3,
                'status_id' => 2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], 
            [
                'reference' => 'PRO-000008',
                'name' => 'Listones de Abeto (30 x 20)',
                'description' => 'Listones de Abeto (30 x 20)',
                'amount' => 50,
                'dimensions' => 'Largo: 2700 mm',
                'weight' => '10 kg',
                'comments' => 'Todos nuestros listones están tratados con pintura para exterior', 
                'category_id' => 4,
                'status_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], 
            [
                'reference' => 'PRO-000009',
                'name' => 'Molduras de Pino Gallego (20 x 10)',
                'description' => 'Molduras de Pino Gallego (20 x 10) en todos los colores',
                'amount' => 20,
                'dimensions' => '20 x 10 mm',
                'weight' => '100 gramos',
                'comments' => 'Todas nuestras molduras están tratadas con pintura para exterior', 
                'category_id' => 4,
                'status_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];        
        
        DB::table('products')->insert($products);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
Use Carbon\Carbon;

class InsertStatusesSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * php artisan db:seed --class=InsertStatusesSeeder
     * 
     * @return void
     */
    public function run()
    {
        $statuses = [
            [ 
                'description' => 'Disponible', 
                'key' => 'available', 
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], 
            [ 
                'description' => 'No Disponible', 
                'key' => 'not_available', 
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [ 
                'description' => 'Activo', 
                'key' => 'active', 
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [ 
                'description' => 'Inactivo', 
                'key' => 'inactive', 
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];        
        
        DB::table('statuses')->insert($statuses);
    }
}

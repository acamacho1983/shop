ConfirmMessage = function(Titulo, Texto, buttonStyle = 'default', parametros = {}) {
    var params = {
        title: Titulo,
        text: Texto,
        type: null,
        allowEscapeKey: false,
        customClass: null,
        allowOutsideClick: null,
        showCancelButton: true,
        showConfirmButton: true,
        confirmButtonClass: 'btn btn-success w-10',
        confirmButtonText: ButtonConfiguration[buttonStyle]['confirmButtonText'],
        confirmButtonColor: ButtonConfiguration[buttonStyle]['confirmButtonColor'],
        cancelButtonClass: 'btn btn-success w-10',
        cancelButtonText: ButtonConfiguration[buttonStyle]['cancelButtonText'],
        cancelButtonColor: ButtonConfiguration[buttonStyle]['cancelButtonColor'],
        reverseButtons: false,
        html: Texto,
        inputClass: 'form-control'
    };

    for(var index in parametros) {
        params[index] = parametros[index];
    };
    return swal(params);
}

AlertMessage = function(Titulo, Texto, buttonStyle = 'default', parametros = {}) {
    var params = {
        title: Titulo,
        text: Texto,
        showCancelButton: false,
        confirmButtonText: ButtonConfiguration[buttonStyle]['confirmButtonText'],
        confirmButtonColor: ButtonConfiguration[buttonStyle]['confirmButtonColor'],
        allowEscapeKey: false,
        allowOutsideClick: null,
        html: Texto
    };

    for(var index in parametros) {
        params[index] = parametros[index];
    };
    return swal(params);
}


var ButtonConfiguration = {
    default: {
        confirmButtonText: 'Aceptar',
        confirmButtonColor: '#65A065',
        cancelButtonText: 'Cancelar',
        cancelButtonColor: '#E64D4D'
    }
}

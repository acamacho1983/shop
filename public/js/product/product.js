function saveProduct(productId)
{    
    $.ajax({
        url: productId ? `/api/product/${productId}` : `/api/product`,
        type: productId ? 'put' : 'post',
        dataType: 'JSON',
        data: {
            description: $('#description').val(),
            amount: $('#amount').val()
        }, 
        success: function (response) { 
            if (response.success) { 
                AlertMessage("", "Datos guardados correctamente").then(function() {
                    window.location.href = `/api/product/${response.data.id}`;
                });
            } else {
                AlertMessage("", response.message);
            }
        }
    });
}
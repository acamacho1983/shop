require('./bootstrap');

window.Vue = require('vue').default;

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import axios from 'axios';

// import VueSweetalert2 from 'vue-sweetalert2';

// const options = {    
//     confirmButtonText: 'Aceptar',
//     confirmButtonColor: '#65A065',
//     cancelButtonText: 'Cancelar',
//     cancelButtonColor: '#E64D4D'
//   };

// Vue.use(VueSweetalert2);

import Controlpanel from './components/controlpanel/controlpanel.vue';
import Navigation from './components/sections/navigation.vue';

import ProductsIndex from './components/products/index.vue';
import ProductCreate from './components/products/create.vue';
import ProductEdit from './components/products/edit.vue';

import ClientsIndex from './components/clients/index.vue';
import ClientCreate from './components/clients/create.vue';
import ClientEdit from './components/clients/edit.vue';

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/api/controlpanel',
            component: Controlpanel,
            name: 'controlpanel.index'
        },
        {
            path: '/api/products',
            component: ProductsIndex,
            name: 'products.index'
        },
        {
            path: '/api/product',
            component: ProductCreate,
            name: 'product.create'
        },
        {
            path: '/api/product/:id',
            component: ProductEdit,
            name: 'product.edit'
        },
        {
            path: '/api/clients',
            component: ClientsIndex,
            name: 'clients.index'
        },
        {
            path: '/api/client',
            component: ClientCreate,
            name: 'client.create'
        },
        {
            path: '/api/client/:id',
            component: ClientEdit,
            name: 'client.edit'
        },
    ]
});

const app = new Vue({
    el: '#app',
    components: {Navigation},
    router
});

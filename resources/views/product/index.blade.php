@extends('sections.head')
<div id="wrapper">
    
    @include('sections.navigation')    

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Productos</h1>
                </div>                
            </div>            
            
            <h4><b>FILTROS</b></h4>
            <div class="panel panel-default">                        
                <div class="panel-body">                        
                    <div class="row">
                        <div class="col-lg-3">
                            <label for="">Referencia</label>
                            <input name="referencia" id="referencia" class="form-control" value=""> 
                        </div>

                        <div class="col-lg-6">
                            <label for="">Descripción</label>
                            <input name="description" id="description" class="form-control" value=""> 
                        </div>

                        <div class="col-lg-3">
                            <label for="">Precio</label>
                            <input name="amount" id="amount" class="form-control text-right" value=""> 
                        </div>
                    </div>                    
                </div>                
            </div>            
            
            <div class="row">
                <div class="col-lg-12">                    
                    <div class="pull-right mb-2">
                        <a class="btn btn-success" href="{{ route('product.new') }}" target="_blank"><i class="fa fa-plus fa-lg"></i> Nuevo</a>
                        <a class="btn btn-info" onclick=""><i class="fa fa-search fa-lg"></i> Buscar</a>
                    </div>                                    
                </div>                
            </div>            
            
            <div class="panel panel-default">                        
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Referencia</th>
                                            <th>Descripción</th>
                                            <th>Precio</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($products as $product) 
                                        <tr>
                                            <td>{{ $product->id }}</td>
                                            <td class = "size-grande">{{ $product->reference }}</td>
                                            <td>{{ $product->description }}</td>
                                            <td class="text-right">{{ $product->amount }} €</td>
                                            <td class="text-center">                                                
                                                <span class="material-icons info-color">                                        
                                                    <a class="text-decoration-none text-info" href="{{ route('product.edit', $product) }}" target="_blank">create</a>
                                                </span>
                                                <span class="material-icons cancel-color">                                        
                                                    <a class="text-decoration-none text-danger" href="#">delete_outline</a>                                        
                                                </span>
                                            </td>
                                        </tr>
                                    @endforeach                                                                                
                                    </tbody>
                                </table>
                            </div>                            
                        </div>
                    </div>
                </div>                
            </div>            
        </div>        
    </div>    
</div>
@extends('sections.footer')
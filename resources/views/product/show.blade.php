@extends('sections.head')
<div id="wrapper">
    
    @include('sections.navigation')

    <div id="page-wrapper">
        <div class="container-fluid">            
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ isset($product ) ? 'Ref.: '.$product->reference() : 'Nuevo Producto' }}</h1>
                </div>                
            </div>            
                        
            <!-- /.row -->
            <div class="row">                
                <div class="panel panel-default">                        
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-9">
                                <label for="">Descripción</label>
                                <input name="description" id="description" class="form-control" value="{{ isset($product) ? $product->description() : '' }}"> 
                            </div>

                            <div class="col-lg-3">
                                <label for="">Precio</label>
                                <input name="amount" id="amount" class="form-control text-right" value="{{ isset($product) ? $product->amount() : '' }}"> 
                            </div>
                        </div>                                                        
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->                
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="pull-right">
                        <button type="button" class="btn btn-success" onclick="saveProduct({{ isset($product) ? $product->id() : null }})"><i class="fa fa-floppy-o"></i> Guardar</button>
                    </div>                                    
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->        
    </div>
    <!-- /#page-wrapper -->

</div>
@push('scripts')
<script src="/js/product/product.js"></script>
@endpush
@extends('sections.footer')
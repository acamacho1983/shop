        <script src="/js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="/js/metisMenu.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <script src="/js/raphael.min.js"></script>
        <script src="/js/morris.min.js"></script>
        <script src="/js/morris-data.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="/js/startmin.js"></script>

        <!-- Alerts -->
        <script src="/js/alerts/alerts.js"></script>
        <script src="/js/alerts/sweetalert2.min.js"></script>

        <script src="{{ asset('js/app.js') }}"></script>

        @stack('scripts')
    </body>
</html>
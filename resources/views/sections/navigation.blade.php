<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="navbar-header">
        <a class="navbar-brand" href="index.html">Tienda</a>
    </div>                              

    <ul class="nav navbar-right navbar-top-links">                    
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> Álvaro Camacho Lugo <b class="caret"></b>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="#"><i class="fa fa-user fa-fw"></i> Perfil</a>
                </li>
                <li><a href="#"><i class="fa fa-gear fa-fw"></i> Configuración</a>
                </li>
                <li class="divider"></li>
                <li><a href="#"><i class="fa fa-sign-out fa-fw"></i> Desconectar</a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">                            
                <li>
                    <a href="#" class="active"><i class="fa fa-product-hunt fa-fw"></i> Productos</a>
                </li>                            
                <li>
                    <a href="#" class="active"><i class="fa fa-users fa-fw"></i> Clientes</a>
                </li>                            
            </ul>
        </div>
    </div>
</nav>
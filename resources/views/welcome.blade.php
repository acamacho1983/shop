@extends('sections.head')
<div id="wrapper">
        
    <div id="app">        
        <div>
            <navigation></navigation>
        </div>

        <div id="page-wrapper">
            <router-view></router-view>
        </div>        
    </div>    

</div>
@extends('sections.footer')
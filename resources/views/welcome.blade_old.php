<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Bienvenido</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        
        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
                <div class="container">
                    <div class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a href="#" class="nav-link">Lista Productos</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">Nuevo Producto</a>
                        </li>
                    </div>    
                </div>
            </nav>
        </div>
    </body>
</html>

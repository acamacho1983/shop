<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Products
Route::get('/products', [App\Http\Controllers\Api\Product\Read\GetProductsController::class, '__invoke'])->name('products.index');
Route::get('/product/{product}', [App\Http\Controllers\Api\Product\Read\FindProductController::class, '__invoke'])->name('product.edit');
Route::put('/product/{product}', [App\Http\Controllers\Api\Product\Update\UpdateProductController::class, '__invoke'])->name('product.update');
Route::delete('/product/{product}', [App\Http\Controllers\Api\Product\Delete\DeleteProductController::class, '__invoke'])->name('product.delete');
Route::post('/product', [App\Http\Controllers\Api\Product\Create\CreateProductController::class, '__invoke'])->name('product.create');

//Clients
Route::get('/clients', [App\Http\Controllers\Api\Client\Read\GetClientsController::class, '__invoke'])->name('clients.index');
Route::get('/client/{client}', [App\Http\Controllers\Api\Client\Read\FindClientController::class, '__invoke'])->name('client.edit');
Route::put('/client/{client}', [App\Http\Controllers\Api\Client\Update\UpdateClientController::class, '__invoke'])->name('client.update');
Route::delete('/client/{client}', [App\Http\Controllers\Api\Client\Delete\DeleteClientController::class, '__invoke'])->name('client.delete');
Route::post('/client', [App\Http\Controllers\Api\Client\Create\CreateClientController::class, '__invoke'])->name('client.create');

//Categories
Route::get('/categories', [App\Http\Controllers\Api\Category\Read\GetCategoriesController::class, '__invoke'])->name('categories.index');

//Statuses
Route::get('/statuses', [App\Http\Controllers\Api\Status\Read\GetStatusesController::class, '__invoke'])->name('statuses.index');
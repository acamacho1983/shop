<?php

namespace Alvaro\Shop\Category\Application\Read;

use Alvaro\Shop\Category\Domain\CategoryEntity;
use Alvaro\Shop\Category\Domain\CategoryRepositoryInterface;

final class FindCategoryUseCase
{
    /**
     * @var CategoryRepositoryInterface $categorytRepository
     */
    private $categorytRepository;

    public function __construct(CategoryRepositoryInterface $categorytRepository)
    {        
        $this->categoryRepository = $categorytRepository;
    }

    public function execute($id)
    {
        return $this->categoryRepository->findCategoryById($id);
    }
}
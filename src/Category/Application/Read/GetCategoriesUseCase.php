<?php

namespace Alvaro\Shop\Category\Application\Read;

use Alvaro\Shop\Category\Domain\CategoryEntity;
use Alvaro\Shop\Category\Domain\CategoryRepositoryInterface;

final class GetCategoriesUseCase
{
    /**
     * @var CategoryRepositoryInterface $categoryRepository
     */
    private $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function execute()
    {                
        return $this->categoryRepository->findAllCategories();
    }
}
<?php

namespace Alvaro\Shop\Category\Domain;
use Alvaro\Shop\Category\Domain\ValueObject\CategoryId;
use Alvaro\Shop\Category\Domain\ValueObject\CategoryDescription;
use Alvaro\Shop\Shared\Infrastructure\Interfaces\Arrayable;

final class CategoryEntity
{   
    private $id;     
    private $description;    
        
    public function __construct(
        CategoryId $id,        
        CategoryDescription $description
    ) {
        $this->id = $id; 
        $this->description = $description;        
    }

    public function id(): int
    {
        return $this->id->get();
    }
    
    public function description(): string
    {
        return $this->description;
    }
    
}

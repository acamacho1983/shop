<?php

namespace Alvaro\Shop\Category\Domain;

interface CategoryRepositoryInterface
{
    /**
     * Persist a new category instance
     *
     * @param Product $category
     *
     * @return void
     */
    public function create(CategoryEntity $category);

    public function update(
        int $id,
        string $description
    ): void;

    public function delete(
        int $id
    ): void;
    
    public function findCategoryById(
         int $id
    );

    public function findAllCategories();
}
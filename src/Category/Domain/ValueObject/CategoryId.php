<?php

namespace Alvaro\Shop\Category\Domain\ValueObject;

class CategoryId 
{

    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function get(): int
    {
        return $this->id;
    }
        
}

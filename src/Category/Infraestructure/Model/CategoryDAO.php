<?php

namespace Alvaro\Shop\Category\Infraestructure\Model;

use Illuminate\Database\Eloquent\Model;
use Alvaro\Shop\Category\Infraestructure\Model\CategoryDAO;

class CategoryDAO extends Model
{
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [        
        'description'
    ];
     

    public function products() {
        return $this->hasMany(CategoryDAO::class);
    }
}
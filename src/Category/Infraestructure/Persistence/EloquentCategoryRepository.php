<?php

namespace Alvaro\Shop\Category\Infraestructure\Persistence;

use Alvaro\Shop\Category\Infraestructure\Model\CategoryDAO;
use Alvaro\Shop\Category\Domain\CategoryEntity;
use Alvaro\Shop\Category\Domain\CategoryRepositoryInterface;
use Alvaro\Shop\Category\Domain\ValueObject\CategoryId;
use Alvaro\Shop\Category\Domain\ValueObject\CategoryDescription;

final class EloquentCategoryRepository implements CategoryRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(CategoryEntity $category)
    {
        $categoryInsert = CategoryDAO::create([ 
            'description'   => $category->description()             
        ]);

        return $categoryInsert;
    }    

    public function update(int $id, string $description): void
    {
        CategoryDAO::where(['id' => $id])
        ->update([            
            'description' => $description,
            'amount' => $amount
        ]);        
    }
    
    public function delete(int $id): void
    {
        CategoryDAO::where(['id' => $id])
        ->delete(); 
    }

    public function findCategoryById(int $id)
    { 
        $dao = CategoryDAO::where([
            'id' => $id])            
        ->first();

        return $dao;
    }

    public function findAllCategories()
    {                
        return CategoryDAO::get();
    }    
    
}
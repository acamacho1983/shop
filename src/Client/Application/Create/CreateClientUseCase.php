<?php

namespace Alvaro\Shop\Client\Application\Create;

use Alvaro\Shop\Client\Domain\ClientEntity;
use Alvaro\Shop\Client\Domain\ValueObject\ClientId;
use Alvaro\Shop\Client\Domain\ValueObject\ClientName;
use Alvaro\Shop\Client\Domain\ValueObject\ClientContactName;
use Alvaro\Shop\Client\Domain\ValueObject\ClientNif;
use Alvaro\Shop\Client\Domain\ValueObject\ClientEmail;
use Alvaro\Shop\Client\Domain\ValueObject\ClientMovil;
use Alvaro\Shop\Client\Domain\ValueObject\ClientPhone;
use Alvaro\Shop\Client\Domain\ValueObject\ClientWeb;
use Alvaro\Shop\Client\Domain\ValueObject\ClientCountry;
use Alvaro\Shop\Client\Domain\ValueObject\ClientProvince;
use Alvaro\Shop\Client\Domain\ValueObject\ClientCity;
use Alvaro\Shop\Client\Domain\ValueObject\ClientAddress;
use Alvaro\Shop\Client\Domain\ValueObject\ClientCp;
use Alvaro\Shop\Client\Domain\ValueObject\ClientStatusId;
use Alvaro\Shop\Client\Domain\ValueObject\ClientComments;
use Alvaro\Shop\Client\Domain\ClientRepositoryInterface;

final class CreateClientUseCase
{
    /**
     * @var ClientRepositoryInterface $clientRepository
     */
    private $clientRepository;

    public function __construct(ClientRepositoryInterface $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function execute($name, $contactName, $nif, $email, $movil, $phone, $web, $country, $province, $city, $address, $cp, $statusId, $comments)
    { 
        $client = new ClientEntity( 
            new ClientId(0),            
            new ClientName($name),
            new ClientContactName($contactName),
            new ClientNif($nif),
            new ClientEmail($email),
            new ClientMovil($movil),
            new ClientPhone($phone),
            new ClientWeb($web),
            new ClientCountry($country),
            new ClientProvince($province),
            new ClientCity($city),
            new ClientAddress($address),
            new ClientCp($cp), 
            new ClientStatusId($statusId),
            new ClientComments($comments)
        ); 

        return $this->clientRepository->create($client);        
    }    
}
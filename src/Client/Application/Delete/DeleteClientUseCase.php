<?php

namespace Alvaro\Shop\Client\Application\Delete;

use Alvaro\Shop\Client\Domain\ClientEntity;
use Alvaro\Shop\Client\Domain\ClientRepositoryInterface;

final class DeleteClientUseCase
{
    /**
     * @var ClientRepositoryInterface $clientRepository
     */
    private $clientRepository;

    public function __construct(ClientRepositoryInterface $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function execute($id)
    {
        $responseClient = $this->clientRepository->findClientById(
            $id
        ); 
        
        if ($responseClient) {
            $this->clientRepository->delete(
                $id
            );

            return $responseClient;
        }
    }
}
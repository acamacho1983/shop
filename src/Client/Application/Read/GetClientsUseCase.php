<?php

namespace Alvaro\Shop\Client\Application\Read;

use Alvaro\Shop\Client\Domain\ClientEntity;
use Alvaro\Shop\Client\Domain\ClientRepositoryInterface;

final class GetClientsUseCase
{
    /**
     * @var ClientRepositoryInterface $clientRepository
     */
    private $clientRepository;

    public function __construct(ClientRepositoryInterface $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function execute($request)
    {                
        return $this->clientRepository->findAllClients($request);
    }
}
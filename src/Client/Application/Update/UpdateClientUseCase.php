<?php

namespace Alvaro\Shop\Client\Application\Update;

use Alvaro\Shop\Client\Domain\ClientEntity;
use Alvaro\Shop\Client\Domain\ClientRepositoryInterface;

final class UpdateClientUseCase
{
    /**
     * @var ClientRepositoryInterface $clientRepository
     */
    private $clientRepository;

    public function __construct(ClientRepositoryInterface $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function execute($id, $name, $contactName, $nif, $email, $movil, $phone, $web, $country, $province, $city, $address, $cp, $statusId, $comments)
    {
        $responseClient = $this->clientRepository->findClientById(
            $id
        ); 
        
        if ($responseClient) {
            $this->clientRepository->update(
                $id,
                $name,
                $contactName ? $contactName : '',
                $nif,
                $email ? $email : '',
                $movil ? $movil : '',
                $phone ? $phone : '',
                $web ? $web : '',
                $country ? $country : '', 
                $province ? $province : '', 
                $city ? $city : '', 
                $address ? $address : '', 
                $cp ? $cp : '',                 
                $statusId,
                $comments ? $comments : ''
            );

            return $responseClient;
        }
    }
}
<?php

namespace Alvaro\Shop\Client\Domain;
use Alvaro\Shop\Client\Domain\ValueObject\ClientId;
use Alvaro\Shop\Client\Domain\ValueObject\ClientName;
use Alvaro\Shop\Client\Domain\ValueObject\ClientContactName;
use Alvaro\Shop\Client\Domain\ValueObject\ClientNif;
use Alvaro\Shop\Client\Domain\ValueObject\ClientEmail;
use Alvaro\Shop\Client\Domain\ValueObject\ClientMovil;
use Alvaro\Shop\Client\Domain\ValueObject\ClientPhone;
use Alvaro\Shop\Client\Domain\ValueObject\ClientWeb;
use Alvaro\Shop\Client\Domain\ValueObject\ClientCountry;
use Alvaro\Shop\Client\Domain\ValueObject\ClientProvince;
use Alvaro\Shop\Client\Domain\ValueObject\ClientCity;
use Alvaro\Shop\Client\Domain\ValueObject\ClientAddress;
use Alvaro\Shop\Client\Domain\ValueObject\ClientCp;
use Alvaro\Shop\Client\Domain\ValueObject\ClientStatusId;
use Alvaro\Shop\Client\Domain\ValueObject\ClientComments;
use Alvaro\Shop\Shared\Infrastructure\Interfaces\Arrayable;

final class ClientEntity
{   
    private $id; 
    private $name;
    private $contactName;
    private $nif; 
    private $email; 
    private $movil; 
    private $phone; 
    private $web; 
    private $country; 
    private $province; 
    private $city; 
    private $address; 
    private $cp; 
    private $statusId; 
    private $comments; 
        
    public function __construct(
        ClientId $id,        
        ClientName $name,
        ClientContactname $contactName,
        ClientNif $nif,
        ClientEmail $email, 
        ClientMovil $movil, 
        ClientPhone $phone, 
        ClientWeb $web, 
        ClientCountry $country, 
        ClientProvince $province, 
        ClientCity $city, 
        ClientAddress $address, 
        ClientCp $cp, 
        ClientStatusId $statusId, 
        ClientComments $comments 
    ) {
        $this->id = $id;        
        $this->name = $name;
        $this->contactName = $contactName;
        $this->nif = $nif;
        $this->email = $email; 
        $this->movil = $movil; 
        $this->phone = $phone; 
        $this->web = $web; 
        $this->country = $country; 
        $this->province = $province; 
        $this->city = $city; 
        $this->address = $address; 
        $this->cp = $cp; 
        $this->statusId = $statusId; 
        $this->comments = $comments; 
    }

    public function id(): int
    {
        return $this->id->get();
    }
    
    public function name(): string
    {
        return $this->name;
    }

    public function contactName(): string
    {
        return $this->contactName;
    }

    public function nif(): string
    {
        return $this->nif;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function movil(): string
    {
        return $this->movil;
    }

    public function phone(): string
    {
        return $this->phone;
    }

    public function web(): string
    {
        return $this->web;
    }

    public function country(): string
    {
        return $this->country;
    }

    public function province(): string
    {
        return $this->province;
    }

    public function city(): string
    {
        return $this->city;
    }

    public function address(): string
    {
        return $this->address;
    }

    public function cp(): string
    {
        return $this->cp;
    } 

    public function statusId(): int
    {
        return $this->statusId->get();
    }

    public function comments(): string
    {
        return $this->comments;
    }        
}

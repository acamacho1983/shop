<?php

namespace Alvaro\Shop\Client\Domain;

interface ClientRepositoryInterface
{
    /**
     * Persist a new client instance
     *
     * @param Client $client
     *
     * @return void
     */
    public function create(ClientEntity $client);

    public function update(
        int $id,
        string $name,
        string $contactName,
        string $nif,
        string $email,
        string $movil,
        string $phone,
        string $web,
        string $country,
        string $province,
        string $city,
        string $address,
        string $cp,        
        int $statusId,
        string $comments
    ): void;    

    public function delete(
        int $id
    ): void;
    
    public function findClientById(
         int $id
    );

    public function findAllClients($request);
}
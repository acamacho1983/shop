<?php

namespace Alvaro\Shop\Client\Domain\ValueObject;

final class ClientAddress
{

    private $address;

    public function __construct($address)
    {
        $this->address = $address;
    }

    public function get(): string
    {
        return $this->address ? $this->address : '';
    }

    public function __toString()
    {
        return $this->address ? $this->address : '';
    }
}
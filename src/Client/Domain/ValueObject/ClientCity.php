<?php

namespace Alvaro\Shop\Client\Domain\ValueObject;

final class ClientCity
{

    private $city;

    public function __construct($city)
    {
        $this->city = $city;
    }

    public function get(): string
    {
        return $this->city ? $this->city : '';
    }

    public function __toString()
    {
        return $this->city ? $this->city : '';
    }
}
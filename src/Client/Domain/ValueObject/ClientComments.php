<?php

namespace Alvaro\Shop\Client\Domain\ValueObject;

final class ClientComments
{

    private $comments;

    public function __construct($comments)
    {
        $this->comments = $comments;
    }

    public function get(): string
    {
        return $this->comments ? $this->comments : '';
    }

    public function __toString()
    {
        return $this->comments ? $this->comments : '';
    }
}
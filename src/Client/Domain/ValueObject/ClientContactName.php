<?php

namespace Alvaro\Shop\Client\Domain\ValueObject;

final class ClientContactName
{

    private $contactName;

    public function __construct($contactName)
    {
        $this->contactName = $contactName;
    }

    public function get(): string
    {
        return $this->contactName ? $this->contactName : '';
    }

    public function __toString()
    {
        return $this->contactName ? $this->contactName : '';
    }
}
<?php

namespace Alvaro\Shop\Client\Domain\ValueObject;

final class ClientCountry
{

    private $country;

    public function __construct($country)
    {
        $this->country = $country;
    }

    public function get(): string
    {
        return $this->country ? $this->country : '';
    }

    public function __toString()
    {
        return $this->country ? $this->country : '';
    }
}
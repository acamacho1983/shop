<?php

namespace Alvaro\Shop\Client\Domain\ValueObject;

final class ClientCp
{

    private $cp;

    public function __construct($cp)
    {
        $this->cp = $cp;
    }

    public function get(): string
    {
        return $this->cp ? $this->cp : '';
    }

    public function __toString()
    {
        return $this->cp ? $this->cp : '';
    }
}
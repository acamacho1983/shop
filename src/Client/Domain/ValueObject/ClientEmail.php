<?php

namespace Alvaro\Shop\Client\Domain\ValueObject;

final class ClientEmail
{

    private $email;

    public function __construct($email)
    {
        $this->email = $email;
    }

    public function get(): string
    {
        return $this->email ? $this->email : '';
    }

    public function __toString()
    {
        return $this->email ? $this->email : '';
    }
}
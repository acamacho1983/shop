<?php

namespace Alvaro\Shop\Client\Domain\ValueObject;

class ClientId 
{

    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function get(): int
    {
        return $this->id;
    }
        
}

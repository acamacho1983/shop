<?php

namespace Alvaro\Shop\Client\Domain\ValueObject;

final class ClientMovil
{

    private $movil;

    public function __construct($movil)
    {
        $this->movil = $movil;
    }

    public function get(): string
    {
        return $this->movil ? $this->movil : '';
    }

    public function __toString()
    {
        return $this->movil ? $this->movil : '';
    }
}
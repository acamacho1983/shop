<?php

namespace Alvaro\Shop\Client\Domain\ValueObject;

final class ClientName
{

    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function get(): string
    { 
        return $this->name;
    }

    public function __toString()
    { 
        return $this->name;
    }
}
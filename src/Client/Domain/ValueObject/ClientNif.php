<?php

namespace Alvaro\Shop\Client\Domain\ValueObject;

final class ClientNif
{

    private $nif;

    public function __construct($nif)
    {
        $this->nif = $nif;
    }

    public function get(): string
    {
        return $this->nif;
    }

    public function __toString()
    {
        return $this->nif;
    }
}
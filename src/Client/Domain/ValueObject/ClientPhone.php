<?php

namespace Alvaro\Shop\Client\Domain\ValueObject;

final class ClientPhone
{

    private $phone;

    public function __construct($phone)
    {
        $this->phone = $phone;
    }

    public function get(): string
    {
        return $this->phone ? $this->phone : '';
    }

    public function __toString()
    {
        return $this->phone ? $this->phone : '';
    }
}
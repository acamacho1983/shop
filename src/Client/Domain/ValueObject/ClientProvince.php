<?php

namespace Alvaro\Shop\Client\Domain\ValueObject;

final class ClientProvince
{

    private $province;

    public function __construct($province)
    {
        $this->province = $province;
    }

    public function get(): string
    {
        return $this->province ? $this->province : '';
    }

    public function __toString()
    {
        return $this->province ? $this->province : '';
    }
}
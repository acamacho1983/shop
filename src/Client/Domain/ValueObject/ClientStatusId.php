<?php

namespace Alvaro\Shop\Client\Domain\ValueObject;

class ClientStatusId 
{

    private $statusId;

    public function __construct(int $statusId)
    {
        $this->statusId = $statusId;
    }

    public function get(): int
    {
        return $this->statusId;
    }
        
}

<?php

namespace Alvaro\Shop\Client\Domain\ValueObject;

final class ClientWeb
{

    private $web;

    public function __construct($web)
    {
        $this->web = $web;
    }

    public function get(): string
    {
        return $this->web ? $this->web : '';
    }

    public function __toString()
    {
        return $this->web ? $this->web : '';
    }
}
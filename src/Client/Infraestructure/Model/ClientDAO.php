<?php

namespace Alvaro\Shop\Client\Infraestructure\Model;

use Illuminate\Database\Eloquent\Model;
use Alvaro\Shop\Status\Infraestructure\Model\StatusDAO;

class ClientDAO extends Model
{
    protected $table = 'clients';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'contact_name',
        'nif',
        'email',
        'movil',
        'phone',
        'web',
        'country',
        'province',
        'city',
        'address',
        'cp',
        'comments', 
        'status_id',
    ];    

    public function status()
    { 
        return $this->hasOne(StatusDAO::class, 'id', 'status_id');
    }
}
<?php

namespace Alvaro\Shop\Client\Infraestructure\Persistence;

use Alvaro\Shop\Client\Infraestructure\Model\ClientDAO;
use Alvaro\Shop\Client\Domain\ClientEntity;
use Alvaro\Shop\Client\Domain\ClientRepositoryInterface;
use Alvaro\Shop\Client\Domain\ValueObject\ClientId;
use Alvaro\Shop\Client\Domain\ValueObject\ClientName;
use Alvaro\Shop\Client\Domain\ValueObject\ClientContactName;
use Alvaro\Shop\Client\Domain\ValueObject\ClientNif;
use Alvaro\Shop\Client\Domain\ValueObject\ClientEmail;
use Alvaro\Shop\Client\Domain\ValueObject\ClientMovil;
use Alvaro\Shop\Client\Domain\ValueObject\ClientPhone;
use Alvaro\Shop\Client\Domain\ValueObject\ClientWeb;
use Alvaro\Shop\Client\Domain\ValueObject\ClientCountry;
use Alvaro\Shop\Client\Domain\ValueObject\ClientProvince;
use Alvaro\Shop\Client\Domain\ValueObject\ClientCity;
use Alvaro\Shop\Client\Domain\ValueObject\ClientAddress;
use Alvaro\Shop\Client\Domain\ValueObject\ClientCp;
use Alvaro\Shop\Client\Domain\ValueObject\ClientStatusId;
use Alvaro\Shop\Client\Domain\ValueObject\ClientComments;
Use Carbon\Carbon;

final class EloquentClientRepository implements ClientRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(ClientEntity $client)
    {
        $clientInsert = ClientDAO::create([                          
            'name' => $client->name(),
            'contact_name' => $client->contactName() ? $client->contactName() : null, 
            'nif' => $client->nif(),
            'email' => $client->email() ? $client->email() : null,
            'movil' => $client->movil() ? $client->movil() : null,
            'phone' => $client->phone() ? $client->phone() : null,
            'web' => $client->web() ? $client->web() : null,
            'country' => $client->country() ? $client->country() : null,
            'province' => $client->province() ? $client->province() : null,
            'city' => $client->city() ? $client->city() : null,
            'address' => $client->address() ? $client->address() : null,
            'cp' => $client->cp() ? $client->cp() : null,
            'status_id' => $client->statusId(),
            'comments' => $client->comments() ? $client->comments() : null
        ]);

        return $clientInsert;
    } 
    
    public function update(int $id, string $name, string $contactName, string $nif, string $email, string $movil, string $phone, string $web, string $country, string $province, string $city, string $address, string $cp, int $statusId, string $comments): void
    {
        ClientDAO::where(['id' => $id])
        ->update([            
            'name' => $name,
            'contact_name' => $contactName,
            'nif' => $nif,
            'email' => $email,
            'movil' => $movil,
            'phone' => $phone,
            'web' => $web,
            'country' => $country,
            'province' => $province,
            'city' => $city,
            'address' => $address,
            'cp' => $cp,
            'status_id' => $statusId,
            'comments' => $comments
        ]);        
    }
    
    public function delete(int $id): void
    {
        ClientDAO::where(['id' => $id])
        ->delete(); 
    }

    public function findClientById(int $id)
    { 
        $dao = ClientDAO::where([
            'id' => $id])            
        ->first();

        return $dao;
    }

    public function findAllClients($request)
    {     
        $query = ClientDAO::with(['status']); 
        
        if ($request->createdAtFrom) {
            $query->where('created_at', '>=', Carbon::createFromFormat('Y-m-d', $request->createdAtFrom)->startOfDay());
        }

        if ($request->createdAtTo) {
            $query->where('created_at', '<=', Carbon::createFromFormat('Y-m-d', $request->createdAtTo)->endOfDay());
        }

        if ($request->statusKey) {
            $query->whereHas('status', function ($q) use ($request) {
                $q->where('key', $request->statusKey);
            });
        }        

        if ($request->name) {
            $query->where('name', 'like', '%'.$request->name.'%')->orWhere('contact_name', 'like', '%'.$request->name.'%');
        }        

        if ($request->cif) {
            $query->where('nif', 'like', '%'.$request->cif.'%');
        }

        if ($request->email) {
            $query->where('email', 'like', '%'.$request->email.'%');
        }

        if ($request->movil) {
            $query->where('movil', 'like', '%'.$request->movil.'%');
        }

        if ($request->province) {
            $query->where('province', 'like', '%'.$request->province.'%');
        }

        if ($request->city) {
            $query->where('city', 'like', '%'.$request->city.'%');
        }        
        
        if ($request->statusId) {
            $query->where('status_id', $request->statusId);
        }

        return $query->get();
    }    
    
}
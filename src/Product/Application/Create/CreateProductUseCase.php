<?php

namespace Alvaro\Shop\Product\Application\Create;

use Alvaro\Shop\Product\Domain\ProductEntity;
use Alvaro\Shop\Product\Domain\ValueObject\ProductId;
use Alvaro\Shop\Product\Domain\ValueObject\ProductReference;
use Alvaro\Shop\Product\Domain\ValueObject\ProductName;
use Alvaro\Shop\Product\Domain\ValueObject\ProductDescription;
use Alvaro\Shop\Product\Domain\ValueObject\ProductAmount;
use Alvaro\Shop\Product\Domain\ValueObject\ProductDimensions;
use Alvaro\Shop\Product\Domain\ValueObject\ProductWeight;
use Alvaro\Shop\Product\Domain\ValueObject\ProductCategoryId;
use Alvaro\Shop\Product\Domain\ValueObject\ProductStatusId;
use Alvaro\Shop\Product\Domain\ValueObject\ProductComments;
use Alvaro\Shop\Product\Domain\ProductRepositoryInterface;

final class CreateProductUseCase
{
    /**
     * @var ProductRepositoryInterface $productRepository
     */
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function execute($name, $description, $amount, $dimensions, $weight, $categoryId, $statusId, $comments)
    { 
        $reference = "PRO-".str_pad(rand(1, 100000), 6, "0", STR_PAD_LEFT); 
        
        $product = new ProductEntity( 
            new ProductId(0),
            new ProductReference($reference),
            new ProductName($name),
            new ProductDescription($description),
            new ProductAmount($amount),
            new ProductDimensions($dimensions),
            new ProductWeight($weight),
            new ProductCategoryId($categoryId),
            new ProductStatusId($statusId),
            new ProductComments($comments)
        ); 

        return $this->productRepository->create($product);        
    }    
}
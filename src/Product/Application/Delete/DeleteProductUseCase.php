<?php

namespace Alvaro\Shop\Product\Application\Delete;

use Alvaro\Shop\Product\Domain\ProductEntity;
use Alvaro\Shop\Product\Domain\ProductRepositoryInterface;

final class DeleteProductUseCase
{
    /**
     * @var ProductRepositoryInterface $productRepository
     */
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function execute($id)
    {
        $responseProduct = $this->productRepository->findProductById(
            $id
        ); 
        
        if ($responseProduct) {
            $this->productRepository->delete(
                $id
            );

            return $responseProduct;
        }
    }
}
<?php

namespace Alvaro\Shop\Product\Application\Read;

use Alvaro\Shop\Product\Domain\ProductEntity;
use Alvaro\Shop\Product\Domain\ProductRepositoryInterface;

final class FindProductUseCase
{
    /**
     * @var ProductRepositoryInterface $productRepository
     */
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {        
        $this->productRepository = $productRepository;
    }

    public function execute($id)
    {
        return $this->productRepository->findProductById($id);
    }
}
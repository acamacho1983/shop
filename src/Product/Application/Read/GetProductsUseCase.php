<?php

namespace Alvaro\Shop\Product\Application\Read;

use Alvaro\Shop\Product\Domain\ProductEntity;
use Alvaro\Shop\Product\Domain\ProductRepositoryInterface;

final class GetProductsUseCase
{
    /**
     * @var ProductRepositoryInterface $productRepository
     */
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function execute($request)
    {                
        return $this->productRepository->findAllProducts($request);
    }
}
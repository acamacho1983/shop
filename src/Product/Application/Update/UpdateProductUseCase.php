<?php

namespace Alvaro\Shop\Product\Application\Update;

// use Alvaro\Shop\Product\Domain\Domain\ValueObject\ProductId;
// use Alvaro\Shop\Product\Domain\Domain\ValueObject\ProductReference;
// use Alvaro\Shop\Product\Domain\Domain\ValueObject\ProductDescription;
// use Alvaro\Shop\Product\Domain\Domain\ValueObject\ProductAmount;
use Alvaro\Shop\Product\Domain\ProductEntity;
use Alvaro\Shop\Product\Domain\ProductRepositoryInterface;

final class UpdateProductUseCase
{
    /**
     * @var ProductRepositoryInterface $productRepository
     */
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function execute($id, $name, $description, $amount, $dimensions, $weight, $categoryId, $statusId, $comments)
    {
        $responseProduct = $this->productRepository->findProductById(
            $id
        ); 
        
        if ($responseProduct) {
            $this->productRepository->update(
                $id,
                $name,
                $description,
                $amount,
                $dimensions ? $dimensions : '',
                $weight ? $weight : '',
                $categoryId,
                $statusId,
                $comments ? $comments : ''
            );

            return $responseProduct;
        }
    }
}
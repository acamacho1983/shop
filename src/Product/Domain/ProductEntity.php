<?php

namespace Alvaro\Shop\Product\Domain;
use Alvaro\Shop\Product\Domain\ValueObject\ProductId;
use Alvaro\Shop\Product\Domain\ValueObject\ProductReference;
use Alvaro\Shop\Product\Domain\ValueObject\ProductName;
use Alvaro\Shop\Product\Domain\ValueObject\ProductDescription;
use Alvaro\Shop\Product\Domain\ValueObject\ProductAmount;
use Alvaro\Shop\Product\Domain\ValueObject\ProductDimensions;
use Alvaro\Shop\Product\Domain\ValueObject\ProductWeight;
use Alvaro\Shop\Product\Domain\ValueObject\ProductCategoryId;
use Alvaro\Shop\Product\Domain\ValueObject\ProductStatusId;
use Alvaro\Shop\Product\Domain\ValueObject\ProductComments;
use Alvaro\Shop\Shared\Infrastructure\Interfaces\Arrayable;

final class ProductEntity
{   
    private $id; 
    private $reference;
    private $name;
    private $description;
    private $amount; 
    private $dimensions; 
    private $weight; 
    private $categoryId; 
    private $statusId; 
    private $comments; 
        
    public function __construct(
        ProductId $id,
        ProductReference $reference,
        ProductName $name,
        ProductDescription $description,
        ProductAmount $amount, 
        ProductDimensions $dimensions, 
        ProductWeight $weight, 
        ProductCategoryId $categoryId, 
        ProductStatusId $statusId, 
        ProductComments $comments 
    ) {
        $this->id = $id;
        $this->reference = $reference;
        $this->name = $name;
        $this->description = $description;
        $this->amount = $amount; 
        $this->dimensions = $dimensions; 
        $this->weight = $weight; 
        $this->categoryId = $categoryId; 
        $this->statusId = $statusId; 
        $this->comments = $comments; 
    }

    public function id(): int
    {
        return $this->id->get();
    }

    public function reference(): string
    {
        return $this->reference;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function description(): string
    {
        return $this->description;
    }

    public function amount(): float
    {
        return $this->amount->get();
    }

    public function dimensions(): string
    {
        return $this->dimensions;
    }

    public function weight(): string
    {
        return $this->weight;
    }

    public function categoryId(): int
    {
        return $this->categoryId->get();
    }

    public function statusId(): int
    {
        return $this->statusId->get();
    }

    public function comments(): string
    {
        return $this->comments;
    }

    // public function toArray()
    // {
    //     return [            
    //         'description'     => $this->description()->get(),
    //         'amount'   => $this->amount(),
    //     ];
    // }
    
}

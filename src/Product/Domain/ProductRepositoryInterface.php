<?php

namespace Alvaro\Shop\Product\Domain;

interface ProductRepositoryInterface
{
    /**
     * Persist a new product instance
     *
     * @param Product $product
     *
     * @return void
     */
    public function create(ProductEntity $product);

    public function update(
        int $id,
        string $name,
        string $description,
        float $amount,
        string $dimensions,
        string $weight,
        int $categoryId,
        int $statusId,
        string $comments
    ): void;

    public function delete(
        int $id
    ): void;
    
    public function findProductById(
         int $id
    );

    public function findAllProducts($request);
}
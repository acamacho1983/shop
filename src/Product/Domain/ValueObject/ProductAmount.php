<?php

namespace Alvaro\Shop\Product\Domain\ValueObject;

class ProductAmount
{

    private $amount;

    public function __construct(float $amount)
    {
        $this->amount = $amount;
    }

    public function get(): float
    {
        return $this->amount;
    }
    
}

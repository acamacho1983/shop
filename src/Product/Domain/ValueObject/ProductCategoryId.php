<?php

namespace Alvaro\Shop\Product\Domain\ValueObject;

class ProductCategoryId 
{

    private $categoryId;

    public function __construct(int $categoryId)
    {
        $this->categoryId = $categoryId;
    }

    public function get(): int
    {
        return $this->categoryId;
    }
        
}

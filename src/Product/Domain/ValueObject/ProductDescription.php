<?php

namespace Alvaro\Shop\Product\Domain\ValueObject;

final class ProductDescription
{

    private $description;

    public function __construct(string $description)
    {
        $this->description = $description;
    }

    public function get(): string
    {
        return $this->description;
    }

    public function __toString()
    {
        return $this->description;
    }
}
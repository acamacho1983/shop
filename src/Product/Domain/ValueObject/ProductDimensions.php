<?php

namespace Alvaro\Shop\Product\Domain\ValueObject;

final class ProductDimensions
{

    private $dimensions;

    public function __construct($dimensions)
    {
        $this->dimensions = $dimensions;
    }

    public function get(): string
    { 
        return $this->dimensions ? $this->dimensions : '';
    }

    public function __toString()
    { 
        return $this->dimensions ? $this->dimensions : '';
    }
}
<?php

namespace Alvaro\Shop\Product\Domain\ValueObject;

class ProductId 
{

    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function get(): int
    {
        return $this->id;
    }
        
}

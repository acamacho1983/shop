<?php

namespace Alvaro\Shop\Product\Domain\ValueObject;

final class ProductName
{

    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function get(): string
    {
        return $this->name;
    }

    public function __toString()
    {
        return $this->name;
    }
}
<?php

namespace Alvaro\Shop\Product\Domain\ValueObject;

final class ProductReference
{

    private $reference;

    public function __construct(string $reference)
    {
        $this->reference = $reference;
    }

    public function get(): string
    {
        return $this->reference;
    }

    public function __toString()
    {
        return $this->reference;
    }
}

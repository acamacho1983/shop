<?php

namespace Alvaro\Shop\Product\Domain\ValueObject;

class ProductStatusId 
{

    private $statusId;

    public function __construct(int $statusId)
    {
        $this->statusId = $statusId;
    }

    public function get(): int
    {
        return $this->statusId;
    }
        
}

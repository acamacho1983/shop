<?php

namespace Alvaro\Shop\Product\Domain\ValueObject;

final class ProductWeight
{

    private $dimensions;

    public function __construct($weight)
    {
        $this->weight = $weight;
    }

    public function get(): string
    {
        return $this->weight ? $this->weight : '';
    }

    public function __toString()
    {
        return $this->weight ? $this->weight : '';
    }
}
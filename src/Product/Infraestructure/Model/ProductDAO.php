<?php

namespace Alvaro\Shop\Product\Infraestructure\Model;

use Illuminate\Database\Eloquent\Model;
use Alvaro\Shop\Category\Infraestructure\Model\CategoryDAO;
use Alvaro\Shop\Status\Infraestructure\Model\StatusDAO;

class ProductDAO extends Model
{
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reference',
        'description',
        'name',
        'amount',
        'dimensions',
        'weight',
        'comments',
        'category_id',
        'status_id',
    ];

    public function category() 
    {
        return $this->hasOne(CategoryDAO::class, 'id', 'category_id');
    }

    public function status()
    { 
        return $this->hasOne(StatusDAO::class, 'id', 'status_id');
    }
}
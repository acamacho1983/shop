<?php

namespace Alvaro\Shop\Product\Infraestructure\Persistence;

use Alvaro\Shop\Product\Infraestructure\Model\ProductDAO;
use Alvaro\Shop\Product\Domain\ProductEntity;
use Alvaro\Shop\Product\Domain\ProductRepositoryInterface;
use Alvaro\Shop\Product\Domain\ValueObject\ProductId;
use Alvaro\Shop\Product\Domain\ValueObject\ProductReference;
use Alvaro\Shop\Product\Domain\ValueObject\ProductName;
use Alvaro\Shop\Product\Domain\ValueObject\ProductDescription;
use Alvaro\Shop\Product\Domain\ValueObject\ProductAmount;
Use Carbon\Carbon;

final class EloquentProductRepository implements ProductRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(ProductEntity $product)
    {
        $productInsert = ProductDAO::create([  
            'reference' => $product->reference(),          
            'description' => $product->description(),
            'name' => $product->name(),
            'amount' => $product->amount(),
            'dimensions' => $product->dimensions() ? $product->dimensions() : null,
            'weight' => $product->weight() ? $product->weight() : null,
            'category_id' => $product->categoryId(),
            'status_id' => $product->statusId(),
            'comments' => $product->comments() ? $product->comments() : null
        ]);

        return $productInsert;
    }    

    public function update(int $id, string $name, string $description, float $amount, string $dimensions, string $weight, int $categoryId, int $statusId, string $comments): void
    {
        ProductDAO::where(['id' => $id])
        ->update([            
            'name' => $name,
            'description' => $description,
            'amount' => $amount,
            'dimensions' => $dimensions,
            'weight' => $weight,
            'category_id' => $categoryId,
            'status_id' => $statusId,
            'comments' => $comments
        ]);        
    }
    
    public function delete(int $id): void
    {
        ProductDAO::where(['id' => $id])
        ->delete(); 
    }

    public function findProductById(int $id)
    { 
        $dao = ProductDAO::where([
            'id' => $id])            
        ->first();

        return $dao;
    }

    public function findAllProducts($request)
    {     
        $query = ProductDAO::with(['category', 'status']); 
        
        if ($request->createdAtFrom) {
            $query->where('created_at', '>=', Carbon::createFromFormat('Y-m-d', $request->createdAtFrom)->startOfDay());
        }

        if ($request->createdAtTo) {
            $query->where('created_at', '<=', Carbon::createFromFormat('Y-m-d', $request->createdAtTo)->endOfDay());
        }

        if ($request->statusKey) {
            $query->whereHas('status', function ($q) use ($request) {
                $q->where('key', $request->statusKey);
            });
        }

        if ($request->reference) {
            $query->where('reference', 'like', '%'.$request->reference.'%');
        }

        if ($request->name) {
            $query->where('name', 'like', '%'.$request->name.'%');
        }

        if ($request->categoryId) {
            $query->where('category_id', $request->categoryId);
        }

        if ($request->statusId) {
            $query->where('status_id', $request->statusId);
        }

        return $query->get();
    }    
    
}
<?php

namespace Alvaro\Shop\Status\Application\Read;

use Alvaro\Shop\Status\Domain\StatusEntity;
use Alvaro\Shop\Status\Domain\StatusRepositoryInterface;

final class FindStatusUseCase
{
    /**
     * @var StatusRepositoryInterface $statustRepository
     */
    private $statustRepository;

    public function __construct(StatusRepositoryInterface $statustRepository)
    {        
        $this->statusRepository = $statustRepository;
    }

    public function execute($id)
    {
        return $this->statusRepository->findStatusById($id);
    }
}
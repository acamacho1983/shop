<?php

namespace Alvaro\Shop\Status\Application\Read;

use Alvaro\Shop\Status\Domain\StatusEntity;
use Alvaro\Shop\Status\Domain\StatusRepositoryInterface;

final class GetStatusesUseCase
{
    /**
     * @var StatusRepositoryInterface $statusRepository
     */
    private $statusRepository;

    public function __construct(StatusRepositoryInterface $statusRepository)
    {
        $this->statusRepository = $statusRepository;
    }

    public function execute($request)
    {                
        return $this->statusRepository->findAllStatuses($request);
    }
}
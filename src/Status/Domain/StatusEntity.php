<?php

namespace Alvaro\Shop\Status\Domain;
use Alvaro\Shop\Status\Domain\ValueObject\StatusId;
use Alvaro\Shop\Status\Domain\ValueObject\StatusDescription;
use Alvaro\Shop\Status\Domain\ValueObject\StatusKey;
use Alvaro\Shop\Shared\Infrastructure\Interfaces\Arrayable;

final class StatusEntity
{   
    private $id;     
    private $description;    
    private $key;    
        
    public function __construct(
        StatusId $id,        
        StatusDescription $description,
        Statuskey $key
    ) {
        $this->id = $id; 
        $this->description = $description; 
        $this->key = $key; 
    }

    public function id(): int
    {
        return $this->id->get();
    }
    
    public function description(): string
    {
        return $this->description;
    }

    public function key(): string
    {
        return $this->key;
    }        
}

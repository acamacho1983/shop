<?php

namespace Alvaro\Shop\Status\Domain;

interface StatusRepositoryInterface
{
    /**
     * Persist a new status instance
     *
     * @param Product $status
     *
     * @return void
     */ 
    public function findStatusById(
         int $id
    );

    public function findAllStatuses($request);
}
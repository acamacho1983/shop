<?php

namespace Alvaro\Shop\Status\Domain\ValueObject;

class StatusId 
{

    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function get(): int
    {
        return $this->id;
    }
        
}

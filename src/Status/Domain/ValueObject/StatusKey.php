<?php

namespace Alvaro\Shop\Status\Domain\ValueObject;

final class StatusKey
{
    private $key;

    public function __construct(string $key)
    {
        $this->key = $key;
    }

    public function get(): string
    {
        return $this->key;
    }

    public function __toString()
    {
        return $this->key;
    }
}
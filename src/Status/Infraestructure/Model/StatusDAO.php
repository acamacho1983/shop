<?php

namespace Alvaro\Shop\Status\Infraestructure\Model;

use Illuminate\Database\Eloquent\Model;
use Alvaro\Shop\Status\Infraestructure\Model\StatusDAO;

class StatusDAO extends Model
{
    protected $table = 'statuses';    
        
}
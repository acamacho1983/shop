<?php

namespace Alvaro\Shop\Status\Infraestructure\Persistence;

use Alvaro\Shop\Status\Infraestructure\Model\StatusDAO;
use Alvaro\Shop\Status\Domain\StatusEntity;
use Alvaro\Shop\Status\Domain\StatusRepositoryInterface;
use Alvaro\Shop\Status\Domain\ValueObject\StatusId;
use Alvaro\Shop\Status\Domain\ValueObject\StatusDescription;
use Alvaro\Shop\Status\Domain\ValueObject\StatusKey;

final class EloquentStatusRepository implements StatusRepositoryInterface
{ 
    public function findStatusById(int $id)
    { 
        $dao = StatusDAO::where([
            'id' => $id])            
        ->first();

        return $dao;
    }

    public function findAllStatuses($request)
    {         
        $query = StatusDAO::query();       

        if (!empty($request->statusesKey)) {
            $query->whereIn('key', $request->statusesKey);
        }
        
        return $query->get();
    }    
    
}
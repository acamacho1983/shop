<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Alvaro\Shop\Client\Infraestructure\Model\ClientDAO;

class ClientManagementTest extends TestCase
{    
    // use RefreshDatabase;    

    /** @test */
    public function a_client_can_be_created()
    {
        $this->withoutExceptionHandling();

        $response = $this->post('/api/client', [
            'name' => 'Test create client name',
            'contact_name' => 'Test create client contact_name',
            'nif' => 'test_nif',
            'email' => 'Test create client email',
            'movil' => 'Test create client movil',
            'phone' => 'Test create client phone',
            'web' => 'Test create client web',
            'country' => 'Test create client country',
            'province' => 'Test create client province',
            'city' => 'Test create client city',
            'address' => 'Test create client address',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => 3
        ]);

        $response->assertOk(); 
        
        $client = ClientDAO::find(json_decode($response->baseResponse->original)->data->id); 

        $this->assertEquals($client->name, 'Test create client name');
        $this->assertEquals($client->contact_name, 'Test create client contact_name');
        $this->assertEquals($client->nif, 'test_nif');
        $this->assertEquals($client->email, 'Test create client email');        
        $this->assertEquals($client->movil, 'Test create client movil');        
        $this->assertEquals($client->phone, 'Test create client phone');        
        $this->assertEquals($client->web, 'Test create client web');        
        $this->assertEquals($client->country, 'Test create client country');        
        $this->assertEquals($client->province, 'Test create client province');        
        $this->assertEquals($client->city, 'Test create client city');        
        $this->assertEquals($client->address, 'Test create client address');        
        $this->assertEquals($client->cp, 'Tescp');        
        $this->assertEquals($client->comments, 'Test create client comments');        
        $this->assertEquals($client->status_id, 3);
    }

    /** @test */
    public function list_of_clients_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        $response = $this->get('/api/clients');

        $response->assertOk(); 
                
        $response->assertSee('Test create client name');
        $response->assertSee('Test create client contact_name');
        $response->assertSee('test_nif');
        $response->assertSee('Test create client email');
        $response->assertSee('Test create client movil');
        $response->assertSee('Test create client phone');
        $response->assertSee('Test create client web');
        $response->assertSee('Test create client country');
        $response->assertSee('Test create client province');
        $response->assertSee('Test create client city');
        $response->assertSee('Test create client address');
        $response->assertSee('Tescp');
        $response->assertSee('Test create client comments');
    }

    /** @test */
    public function a_client_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        $client = ClientDAO::orderBy('id', 'desc')->first();

        $response = $this->get('/api/client/'.$client->id); 

        $this->assertEquals($response->baseResponse->original->name, 'Test create client name');
        $this->assertEquals($response->baseResponse->original->contact_name, 'Test create client contact_name');
        $this->assertEquals($response->baseResponse->original->nif, 'test_nif');
        $this->assertEquals($response->baseResponse->original->email, 'Test create client email');        
        $this->assertEquals($response->baseResponse->original->movil, 'Test create client movil');        
        $this->assertEquals($response->baseResponse->original->phone, 'Test create client phone');        
        $this->assertEquals($response->baseResponse->original->web, 'Test create client web');        
        $this->assertEquals($response->baseResponse->original->country, 'Test create client country');        
        $this->assertEquals($response->baseResponse->original->province, 'Test create client province');        
        $this->assertEquals($response->baseResponse->original->city, 'Test create client city');        
        $this->assertEquals($response->baseResponse->original->address, 'Test create client address');        
        $this->assertEquals($response->baseResponse->original->cp, 'Tescp');        
        $this->assertEquals($response->baseResponse->original->comments, 'Test create client comments');        
        $this->assertEquals($response->baseResponse->original->status_id, 3);
    }

    /** @test */
    public function a_client_can_be_updated()
    {
        //$this->withoutExceptionHandling();

        $client = ClientDAO::orderBy('id', 'desc')->first(); 

        $response = $this->put('/api/client/'.$client->id, [ 
            'name' => $client->name.' -- before update',
            'contact_name' => $client->contact_name.' -- before update', 
            'nif' => 'nifbefupd', 
            'email' => $client->email.' -- before update', 
            'movil' => $client->movil.' -- before update', 
            'phone' => $client->phone.' -- before update', 
            'web' => $client->web.' -- before update', 
            'country' => $client->country.' -- before update', 
            'province' => $client->province.' -- before update', 
            'city' => $client->city.' -- before update', 
            'address' => $client->address.' -- before update', 
            'cp' => ' befup', 
            'comments' => $client->comments.' -- before update', 
            'status_id' => 4          
        ]); 

        $response->assertOk(); 

        $clientUpdate = ClientDAO::find($client->id);
                
        $this->assertEquals($clientUpdate->name, $client->name.' -- before update'); 
        $this->assertEquals($clientUpdate->contact_name, $client->contact_name.' -- before update'); 
        $this->assertEquals($clientUpdate->nif, 'nifbefupd'); 
        $this->assertEquals($clientUpdate->email, $client->email.' -- before update'); 
        $this->assertEquals($clientUpdate->movil, $client->movil.' -- before update'); 
        $this->assertEquals($clientUpdate->phone, $client->phone.' -- before update'); 
        $this->assertEquals($clientUpdate->web, $client->web.' -- before update'); 
        $this->assertEquals($clientUpdate->country, $client->country.' -- before update'); 
        $this->assertEquals($clientUpdate->province, $client->province.' -- before update'); 
        $this->assertEquals($clientUpdate->city, $client->city.' -- before update'); 
        $this->assertEquals($clientUpdate->address, $client->address.' -- before update'); 
        $this->assertEquals($clientUpdate->cp, 'befup'); 
        $this->assertEquals($clientUpdate->comments, $client->comments.' -- before update'); 
        $this->assertEquals($clientUpdate->status_id, 4);         
    }

    /** @test */
    public function a_client_can_be_deleted()
    {
        $this->withoutExceptionHandling(); 

        $response = $this->post('/api/client', [
            'name' => 'Test create client name',
            'contact_name' => 'Test create client contact_name',
            'nif' => 'test_nif',
            'email' => 'Test create client email',
            'movil' => 'Test create client movil',
            'phone' => 'Test create client phone',
            'web' => 'Test create client web',
            'country' => 'Test create client country',
            'province' => 'Test create client province',
            'city' => 'Test create client city',
            'address' => 'Test create client address',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => 3
        ]);

        $response->assertOk(); 
        
        $response = $this->delete('/api/client/'.json_decode($response->baseResponse->original)->data->id); 

        $response->assertOk(); 
    }

    /** @test */
    public function client_name_is_required()
    { 
        $response = $this->post('/api/client', [
            'name' => '',
            'contact_name' => 'Test create client contact_name',
            'nif' => 'test_nif',
            'email' => 'Test create client email',
            'movil' => 'Test create client movil',
            'phone' => 'Test create client phone',
            'web' => 'Test create client web',
            'country' => 'Test create client country',
            'province' => 'Test create client province',
            'city' => 'Test create client city',
            'address' => 'Test create client address',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => 3
        ]);

        $response->assertSessionHasErrors(['name']); 
    }

    /** @test */
    public function client_name_min_3_characters()
    { 
        $response = $this->post('/api/client', [
            'name' => 'Al',
            'contact_name' => 'Test create client contact_name',
            'nif' => 'test_nif',
            'email' => 'Test create client email',
            'movil' => 'Test create client movil',
            'phone' => 'Test create client phone',
            'web' => 'Test create client web',
            'country' => 'Test create client country',
            'province' => 'Test create client province',
            'city' => 'Test create client city',
            'address' => 'Test create client address',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => 3
        ]);

        $response->assertSessionHasErrors(['name']); 
    }

    /** @test */
    public function client_name_max_255_characters()
    {         
        $response = $this->post('/api/client', [
            'name' => 'ALorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industria',
            'contact_name' => 'Test create client contact_name',
            'nif' => 'test_nif',
            'email' => 'Test create client email',
            'movil' => 'Test create client movil',
            'phone' => 'Test create client phone',
            'web' => 'Test create client web',
            'country' => 'Test create client country',
            'province' => 'Test create client province',
            'city' => 'Test create client city',
            'address' => 'Test create client address',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => 3
        ]);

        $response->assertSessionHasErrors(['name']); 
    }

    /** @test */
    public function client_contact_name_is_required()
    { 
        $response = $this->post('/api/client', [
            'name' => 'Test create client name',
            'contact_name' => '',
            'nif' => 'test_nif',
            'email' => 'Test create client email',
            'movil' => 'Test create client movil',
            'phone' => 'Test create client phone',
            'web' => 'Test create client web',
            'country' => 'Test create client country',
            'province' => 'Test create client province',
            'city' => 'Test create client city',
            'address' => 'Test create client address',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => 3
        ]);

        $response->assertSessionHasErrors(['contact_name']); 
    }

    /** @test */
    public function client_contact_name_min_3_characters()
    { 
        $response = $this->post('/api/client', [
            'name' => 'Test create client name',
            'contact_name' => 'Al',
            'nif' => 'test_nif',
            'email' => 'Test create client email',
            'movil' => 'Test create client movil',
            'phone' => 'Test create client phone',
            'web' => 'Test create client web',
            'country' => 'Test create client country',
            'province' => 'Test create client province',
            'city' => 'Test create client city',
            'address' => 'Test create client address',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => 3
        ]);

        $response->assertSessionHasErrors(['contact_name']); 
    }

    /** @test */
    public function client_contact_name_max_255_characters()
    {         
        $response = $this->post('/api/client', [
            'name' => 'Test create client name',
            'contact_name' => 'ALorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó u',
            'nif' => 'test_nif',
            'email' => 'Test create client email',
            'movil' => 'Test create client movil',
            'phone' => 'Test create client phone',
            'web' => 'Test create client web',
            'country' => 'Test create client country',
            'province' => 'Test create client province',
            'city' => 'Test create client city',
            'address' => 'Test create client address',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => 3
        ]);

        $response->assertSessionHasErrors(['contact_name']); 
    }

    /** @test */
    public function client_nif_is_required()
    { 
        $response = $this->post('/api/client', [
            'name' => 'Test create client name',
            'contact_name' => 'Test create client contact_name',
            'nif' => '',
            'email' => 'Test create client email',
            'movil' => 'Test create client movil',
            'phone' => 'Test create client phone',
            'web' => 'Test create client web',
            'country' => 'Test create client country',
            'province' => 'Test create client province',
            'city' => 'Test create client city',
            'address' => 'Test create client address',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => 3
        ]);

        $response->assertSessionHasErrors(['nif']);  
    }

    /** @test */
    public function client_nif_max_15_characters()
    { 
        $response = $this->post('/api/client', [
            'name' => 'Test create client name',
            'contact_name' => 'Test create client contact_name',
            'nif' => 'A75999888777666A',
            'email' => 'Test create client email',
            'movil' => 'Test create client movil',
            'phone' => 'Test create client phone',
            'web' => 'Test create client web',
            'country' => 'Test create client country',
            'province' => 'Test create client province',
            'city' => 'Test create client city',
            'address' => 'Test create client address',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => 3
        ]);

        $response->assertSessionHasErrors(['nif']);  
    }

    /** @test */
    public function client_email_max_150_characters()
    { 
        $response = $this->post('/api/client', [
            'name' => 'Test create client name',
            'contact_name' => 'Test create client contact_name',
            'nif' => 'testnif',
            'email' => 'ALorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industria',
            'movil' => 'Test create client movil',
            'phone' => 'Test create client phone',
            'web' => 'Test create client web',
            'country' => 'Test create client country',
            'province' => 'Test create client province',
            'city' => 'Test create client city',
            'address' => 'Test create client address',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => 3
        ]);

        $response->assertSessionHasErrors(['email']);  
    }

    /** @test */
    public function client_movil_max_50_characters()
    {         
        $response = $this->post('/api/client', [
            'name' => 'Test create client name',
            'contact_name' => 'Test create client contact_name',
            'nif' => 'testnif',
            'email' => 'Test create client email',
            'movil' => 'AAAAAAAAAAAAAAAAAAAAAAAAAAATest create client movil',
            'phone' => 'Test create client phone',
            'web' => 'Test create client web',
            'country' => 'Test create client country',
            'province' => 'Test create client province',
            'city' => 'Test create client city',
            'address' => 'Test create client address',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => 3
        ]);

        $response->assertSessionHasErrors(['movil']);  
    }

    /** @test */
    public function client_phone_max_50_characters()
    {         
        $response = $this->post('/api/client', [
            'name' => 'Test create client name',
            'contact_name' => 'Test create client contact_name',
            'nif' => 'testnif',
            'email' => 'Test create client email',
            'movil' => 'Test create client movil',
            'phone' => 'AAAAAAAAAAAAAAAAAAAAAAAAAAATest create client phone',
            'web' => 'Test create client web',
            'country' => 'Test create client country',
            'province' => 'Test create client province',
            'city' => 'Test create client city',
            'address' => 'Test create client address',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => 3
        ]);

        $response->assertSessionHasErrors(['phone']);  
    }

    /** @test */
    public function client_web_max_150_characters()
    {         
        $response = $this->post('/api/client', [
            'name' => 'Test create client name',
            'contact_name' => 'Test create client contact_name',
            'nif' => 'testnif',
            'email' => 'Test create client email',
            'movil' => 'Test create client movil',
            'phone' => 'Test create client phone',
            'web' => 'ALorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industria',
            'country' => 'Test create client country',
            'province' => 'Test create client province',
            'city' => 'Test create client city',
            'address' => 'Test create client address',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => 3
        ]);

        $response->assertSessionHasErrors(['web']);  
    }

    /** @test */
    public function client_country_max_100_characters()
    {
        $response = $this->post('/api/client', [
            'name' => 'Test create client name',
            'contact_name' => 'Test create client contact_name',
            'nif' => 'testnif',
            'email' => 'Test create client email',
            'movil' => 'Test create client movil',
            'phone' => 'Test create client phone',
            'web' => 'Test create client web',
            'country' => 'AALorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha',
            'province' => 'Test create client province',
            'city' => 'Test create client city',
            'address' => 'Test create client address',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => 3
        ]);

        $response->assertSessionHasErrors(['country']);  
    }

    /** @test */
    public function client_province_max_100_characters()
    {        
        $response = $this->post('/api/client', [
            'name' => 'Test create client name',
            'contact_name' => 'Test create client contact_name',
            'nif' => 'testnif',
            'email' => 'Test create client email',
            'movil' => 'Test create client movil',
            'phone' => 'Test create client phone',
            'web' => 'Test create client web',
            'country' => 'Test create client country',
            'province' => 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha',
            'city' => 'Test create client city',
            'address' => 'Test create client address',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => 3
        ]);

        $response->assertSessionHasErrors(['province']);  
    }

    /** @test */
    public function client_city_max_200_characters()
    {        
        $response = $this->post('/api/client', [
            'name' => 'Test create client name',
            'contact_name' => 'Test create client contact_name',
            'nif' => 'testnif',
            'email' => 'Test create client email',
            'movil' => 'Test create client movil',
            'phone' => 'Test create client phone',
            'web' => 'Test create client web',
            'country' => 'Test create client country',
            'province' => 'Test create client province',
            'city' => 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industria',
            'address' => 'Test create client address',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => 3
        ]);

        $response->assertSessionHasErrors(['city']);  
    }

    /** @test */
    public function client_address_max_255_characters()
    {        
        $response = $this->post('/api/client', [
            'name' => 'Test create client name',
            'contact_name' => 'Test create client contact_name',
            'nif' => 'testnif',
            'email' => 'Test create client email',
            'movil' => 'Test create client movil',
            'phone' => 'Test create client phone',
            'web' => 'Test create client web',
            'country' => 'Test create client country',
            'province' => 'Test create client province',
            'city' => 'Test create client province',
            'address' => 'ALorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó u',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => 3
        ]);

        $response->assertSessionHasErrors(['address']);  
    }

    /** @test */
    public function client_cp_max_5_characters()
    {        
        $response = $this->post('/api/client', [
            'name' => 'Test create client name',
            'contact_name' => 'Test create client contact_name',
            'nif' => 'testnif',
            'email' => 'Test create client email',
            'movil' => 'Test create client movil',
            'phone' => 'Test create client phone',
            'web' => 'Test create client web',
            'country' => 'Test create client country',
            'province' => 'Test create client province',
            'city' => 'Test create client province',
            'address' => 'Test create client province',
            'cp' => '290091',
            'comments' => 'Test create client comments',            
            'status_id' => 3
        ]);

        $response->assertSessionHasErrors(['cp']);  
    }

    /** @test */
    public function client_status_id_is_required()
    { 
        $response = $this->post('/api/client', [
            'name' => 'Test create client name',
            'contact_name' => 'Test create client contact_name',
            'nif' => '',
            'email' => 'Test create client email',
            'movil' => 'Test create client movil',
            'phone' => 'Test create client phone',
            'web' => 'Test create client web',
            'country' => 'Test create client country',
            'province' => 'Test create client province',
            'city' => 'Test create client city',
            'address' => 'Test create client address',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => ''
        ]);

        $response->assertSessionHasErrors(['status_id']); 
    }

    /** @test */
    public function client_status_id_exist_statuses_table()
    { 
        $response = $this->post('/api/client', [
            'name' => 'Test create client name',
            'contact_name' => 'Test create client contact_name',
            'nif' => '',
            'email' => 'Test create client email',
            'movil' => 'Test create client movil',
            'phone' => 'Test create client phone',
            'web' => 'Test create client web',
            'country' => 'Test create client country',
            'province' => 'Test create client province',
            'city' => 'Test create client city',
            'address' => 'Test create client address',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => 11
        ]);

        $response->assertSessionHasErrors(['status_id']); 
    }
}

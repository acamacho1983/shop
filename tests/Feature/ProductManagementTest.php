<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Alvaro\Shop\Product\Infraestructure\Model\ProductDAO;

class ProductManagementTest extends TestCase
{    
    // use RefreshDatabase;    

    /** @test */
    public function a_product_can_be_created()
    {
        $this->withoutExceptionHandling();

        $response = $this->post('/api/product', [
            'name' => 'Test create product name',
            'description' => 'Test create product description',
            'amount' => 10,
            'dimensions' => 'Test create product dimensions',
            'weight' => 'Test create product weight',
            'comments' => 'Test create product comments',
            'category_id' => 1,
            'status_id' => 1
        ]);

        $response->assertOk(); 

        $product = ProductDAO::find(json_decode($response->baseResponse->original)->data->id); 
                
        $this->assertEquals($product->name, 'Test create product name');
        $this->assertEquals($product->description, 'Test create product description');
        $this->assertEquals($product->amount, 10);
        $this->assertEquals($product->dimensions, 'Test create product dimensions');
        $this->assertEquals($product->weight, 'Test create product weight');
        $this->assertEquals($product->comments, 'Test create product comments');
        $this->assertEquals($product->category_id, 1);
        $this->assertEquals($product->status_id, 1);
    }

    /** @test */
    public function list_of_products_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        $response = $this->get('/api/products');

        $response->assertOk(); 
                
        $response->assertSee('Test create product name');
        $response->assertSee('Test create product description');
        $response->assertSee('Test create product dimensions');
        $response->assertSee('Test create product weight');
        $response->assertSee('Test create product comments');
    }

    /** @test */
    public function a_product_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        $product = ProductDAO::orderBy('id', 'desc')->first();

        $response = $this->get('/api/product/'.$product->id);

        $this->assertEquals($response->baseResponse->original->name, 'Test create product name');
        $this->assertEquals($response->baseResponse->original->description, 'Test create product description');
        $this->assertEquals($response->baseResponse->original->amount, 10);
        $this->assertEquals($response->baseResponse->original->dimensions, 'Test create product dimensions');
        $this->assertEquals($response->baseResponse->original->weight, 'Test create product weight');
        $this->assertEquals($response->baseResponse->original->comments, 'Test create product comments');
        $this->assertEquals($response->baseResponse->original->category_id, 1);
        $this->assertEquals($response->baseResponse->original->status_id, 1);
    }

    /** @test */
    public function a_product_can_be_updated()
    {
        $this->withoutExceptionHandling();

        $product = ProductDAO::orderBy('id', 'desc')->first(); 

        $response = $this->put('/api/product/'.$product->id, [ 
            'name' => $product->name.' -- before update',
            'description' => $product->description.' -- before update',
            'amount' => 15,
            'dimensions' => $product->dimensions.' -- before update',
            'weight' => $product->weight.' -- before update',
            'comments' => $product->comments.' -- before update',
            'category_id' => 2,
            'status_id' => 2            
        ]); 

        $response->assertOk(); 

        $productUpdate = ProductDAO::find($product->id);
                
        $this->assertEquals($productUpdate->name, $product->name.' -- before update');
        $this->assertEquals($productUpdate->description, $product->description.' -- before update');
        $this->assertEquals($productUpdate->amount, 15);
        $this->assertEquals($productUpdate->dimensions, $product->dimensions.' -- before update');
        $this->assertEquals($productUpdate->weight, $product->weight.' -- before update');
        $this->assertEquals($productUpdate->comments, $product->comments.' -- before update');
        $this->assertEquals($productUpdate->category_id, 2);
        $this->assertEquals($productUpdate->status_id, 2);
    }

    /** @test */
    public function a_product_can_be_deleted()
    {
        $this->withoutExceptionHandling(); 

        $product = ProductDAO::orderBy('id', 'desc')->first(); 
        
        $response = $this->delete('/api/product/'.$product->id); 

        $response->assertOk();         
    }

    /** @test */
    public function product_name_is_required()
    { 
        $response = $this->post('/api/product', [
            'name' => '',
            'description' => 'Test create product description',
            'amount' => 10, 
            'category_id' => 1,
            'status_id' => 1
        ]);

        $response->assertSessionHasErrors(['name']); 
    }

    /** @test */
    public function product_name_min_3_characters()
    { 
        $response = $this->post('/api/product', [
            'name' => 'Al',
            'description' => 'Test create product description',
            'amount' => 10, 
            'category_id' => 1,
            'status_id' => 1
        ]);

        $response->assertSessionHasErrors(['name']); 
    }

    /** @test */
    public function product_name_max_150_characters()
    {        
        $response = $this->post('/api/product', [
            'name' => 'ALorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industria',
            'description' => 'Test create product description',
            'amount' => 10, 
            'category_id' => 1,
            'status_id' => 1
        ]);        
        
        $response->assertSessionHasErrors(['name']); 
    }

    /** @test */
    public function product_description_is_required()
    { 
        $response = $this->post('/api/product', [
            'name' => 'Test create product name',
            'description' => '',
            'amount' => 10, 
            'category_id' => 1,
            'status_id' => 1
        ]);

        $response->assertSessionHasErrors(['description']); 
    }

    /** @test */
    public function product_description_min_3_characters()
    { 
        $response = $this->post('/api/product', [
            'name' => 'Test create product name',
            'description' => 'Al',
            'amount' => 10, 
            'category_id' => 1,
            'status_id' => 1
        ]);

        $response->assertSessionHasErrors(['description']); 
    }

    /** @test */
    public function product_description_max_255_characters()
    { 
        $response = $this->post('/api/product', [
            'name' => 'Test create product name',
            'description' => 'ALorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó u',
            'amount' => 10, 
            'category_id' => 1,
            'status_id' => 1
        ]);

        $response->assertSessionHasErrors(['description']); 
    }

    /** @test */
    public function product_amount_is_numeric()
    { 
        $response = $this->post('/api/product', [
            'name' => 'Test create product name',
            'description' => 'Test create product description',
            'amount' => 'a', 
            'category_id' => 1,
            'status_id' => 1
        ]);

        $response->assertSessionHasErrors(['amount']); 
    }

    /** @test */
    public function product_amount_greater_than_zero()
    { 
        $response = $this->post('/api/product', [
            'name' => 'Test create product name',
            'description' => 'Test create product description',
            'amount' => 0, 
            'category_id' => 1,
            'status_id' => 1
        ]);

        $response->assertSessionHasErrors(['amount']); 
    }

    /** @test */
    public function product_dimensions_max_150_characters()
    { 
        $response = $this->post('/api/product', [
            'name' => 'Test create product name',
            'description' => 'Test create product description',
            'dimensions' => 'ALorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industria',
            'amount' => 10, 
            'category_id' => 1,
            'status_id' => 1
        ]);        

        $response->assertSessionHasErrors(['dimensions']);  
    }

    /** @test */
    public function product_weight_max_150_characters()
    { 
        $response = $this->post('/api/product', [
            'name' => 'Test create product name',
            'description' => 'Test create product description',
            'weight' => 'ALorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industria',
            'amount' => 10, 
            'category_id' => 1,
            'status_id' => 1
        ]);        

        $response->assertSessionHasErrors(['weight']);  
    }

    /** @test */
    public function client_email_max_150_characters()
    { 
        $response = $this->post('/api/client', [
            'name' => 'Test create client name',
            'contact_name' => 'Test create client contact_name',
            'nif' => 'testnif',
            'email' => 'ALorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industria',
            'movil' => 'Test create client movil',
            'phone' => 'Test create client phone',
            'web' => 'Test create client web',
            'country' => 'Test create client country',
            'province' => 'Test create client province',
            'city' => 'Test create client city',
            'address' => 'Test create client address',
            'cp' => 'Tescp',
            'comments' => 'Test create client comments',            
            'status_id' => 3
        ]);

        $response->assertSessionHasErrors(['email']);  
    }

    /** @test */
    public function product_category_id_is_required()
    { 
        $response = $this->post('/api/product', [
            'name' => 'Test create product name',
            'description' => 'Test create product description',
            'amount' => 0, 
            'category_id' => '',
            'status_id' => 1
        ]);

        $response->assertSessionHasErrors(['category_id']); 
    }

    /** @test */
    public function product_category_id_exist_categories_table()
    { 
        $response = $this->post('/api/product', [
            'name' => 'Test create product name',
            'description' => 'Test create product description',
            'amount' => 0, 
            'category_id' => 8,
            'status_id' => 1
        ]);

        $response->assertSessionHasErrors(['category_id']); 
    }

    /** @test */
    public function product_status_id_is_required()
    { 
        $response = $this->post('/api/product', [
            'name' => 'Test create product name',
            'description' => 'Test create product description',
            'amount' => 0, 
            'category_id' => 1,
            'status_id' => ''
        ]);

        $response->assertSessionHasErrors(['status_id']); 
    }

    /** @test */
    public function product_status_id_exist_statuses_table()
    { 
        $response = $this->post('/api/product', [
            'name' => 'Test create product name',
            'description' => 'Test create product description',
            'amount' => 0, 
            'category_id' => 1,
            'status_id' => 11
        ]);

        $response->assertSessionHasErrors(['status_id']); 
    }
}

<?php

namespace Tests\Unit;

use Tests\TestCase;
use Alvaro\Shop\Category\Infraestructure\Model\CategoryDAO;
use Illuminate\Database\Eloquent\Collection;

class CategoryTest extends TestCase
{    
    public function test_a_category_has_many_products()
    {
        $category = new CategoryDAO;

        $this->assertInstanceOf(Collection::class, $category->products);

    }
}
